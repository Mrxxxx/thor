package threads.magnet.net.pipeline;

import java.nio.ByteBuffer;

import threads.magnet.net.Peer;
import threads.magnet.protocol.EncodingContext;
import threads.magnet.protocol.Message;
import threads.magnet.protocol.handler.MessageHandler;

public record MessageSerializer(EncodingContext context, MessageHandler<Message> protocol) {


    public static MessageSerializer createMessageSerializer(Peer peer,
                                                            MessageHandler<Message> protocol) {
        return new MessageSerializer(new EncodingContext(peer), protocol);
    }

    public boolean serialize(Message message, ByteBuffer buffer) {
        return protocol.encode(context, message, buffer);
    }
}
