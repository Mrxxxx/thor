package threads.magnet.net;

import java.io.IOException;

import threads.magnet.data.Bitfield;
import threads.magnet.protocol.BitOrder;
import threads.magnet.protocol.Handshake;
import threads.magnet.torrent.TorrentDescriptor;
import threads.magnet.torrent.TorrentRegistry;

public record BitfieldConnectionHandler(
        TorrentRegistry torrentRegistry) implements HandshakeHandler {

    @Override
    public void processIncomingHandshake(PeerConnection connection, Handshake peerHandshake) {
        TorrentDescriptor descriptorOptional = torrentRegistry.getDescriptor(connection.getTorrentId());
        if (descriptorOptional != null && descriptorOptional.isActive()
                && descriptorOptional.getDataDescriptor() != null) {
            Bitfield bitfield = descriptorOptional.getDataDescriptor().getBitfield();

            if (bitfield.getPiecesComplete() > 0) {
                Peer peer = connection.getRemotePeer();
                threads.magnet.protocol.Bitfield bitfieldMessage = new threads.magnet.protocol.Bitfield(bitfield.toByteArray(BitOrder.LITTLE_ENDIAN));
                try {
                    connection.postMessage(bitfieldMessage);
                } catch (IOException e) {
                    throw new RuntimeException("Failed to send bitfield to peer: " + peer, e);
                }
            }
        }
    }

    @Override
    public void processOutgoingHandshake(Handshake handshake) {
        // do nothing
    }
}
