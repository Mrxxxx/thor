package threads.magnet.torrent;

import java.util.function.IntPredicate;
import java.util.stream.IntStream;

public record ValidatingSelector(IntPredicate validator,
                                 PieceSelector delegate) implements PieceSelector {

    @Override
    public IntStream getNextPieces(PieceStatistics pieceStatistics) {
        return delegate.getNextPieces(pieceStatistics)
                .filter(validator);
    }
}
