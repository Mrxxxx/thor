package threads.magnet.processor;

import threads.magnet.event.EventSink;
import threads.magnet.torrent.TorrentDescriptor;
import threads.magnet.torrent.TorrentRegistry;

public record TorrentContextFinalizer(TorrentRegistry torrentRegistry,
                                      EventSink eventSink) implements ContextFinalizer {

    @Override
    public void finalizeContext(MagnetContext context) {
        TorrentDescriptor torrentDescriptor = torrentRegistry.getDescriptor(context.getTorrentId());
        if (torrentDescriptor != null) {
            torrentDescriptor.stop();
        }
        eventSink.fireTorrentStopped(context.getTorrentId());
    }
}
