package threads.magnet.processor;

import androidx.annotation.NonNull;

import java.util.Objects;

import threads.magnet.event.EventSink;
import threads.magnet.metainfo.TorrentId;
import threads.magnet.torrent.TorrentDescriptor;
import threads.magnet.torrent.TorrentRegistry;

public class ProcessTorrentStage extends TerminateOnErrorProcessingStage {

    private final TorrentRegistry torrentRegistry;

    private final EventSink eventSink;

    ProcessTorrentStage(TorrentRegistry torrentRegistry, EventSink eventSink) {
        super(null);
        this.torrentRegistry = torrentRegistry;
        this.eventSink = eventSink;
    }

    @Override
    protected void doExecute(MagnetContext context) {
        TorrentId torrentId = context.getTorrentId();
        TorrentDescriptor descriptor = getDescriptor(torrentId);

        descriptor.start();

        eventSink.fireTorrentStarted(torrentId);

        while (descriptor.isActive()) {
            if (Objects.requireNonNull(context.getState()).getPiecesRemaining() == 0) {
                break;
            }
        }
    }


    @NonNull
    private TorrentDescriptor getDescriptor(TorrentId torrentId) {
        TorrentDescriptor td = torrentRegistry.getDescriptor(torrentId);
        if (td == null) {
            throw new IllegalStateException("No descriptor present for threads.torrent ID: " + torrentId);
        }
        return td;
    }

    @Override
    public ProcessingEvent after() {
        return ProcessingEvent.DOWNLOAD_COMPLETE;
    }
}
