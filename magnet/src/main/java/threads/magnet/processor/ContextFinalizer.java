package threads.magnet.processor;

interface ContextFinalizer {

    void finalizeContext(MagnetContext context);
}
