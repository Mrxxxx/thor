package threads.magnet.data.range;

import java.util.concurrent.locks.ReentrantLock;

import threads.magnet.data.BlockSet;

public interface Ranges {


    static BlockRange blockRange(Range range, long blockSize) {
        return BlockRange.createBlockRange(range, blockSize);
    }

    static SynchronizedRange synchronizedRange(Range range) {
        return SynchronizedRange.createSynchronizedRange(range);
    }

    static BlockSet synchronizedBlockSet(BlockSet blockSet) {
        return new SynchronizedBlockSet(blockSet, new ReentrantLock());
    }
}
