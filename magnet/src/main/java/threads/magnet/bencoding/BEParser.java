package threads.magnet.bencoding;

/**
 * BEncoding parser. Should be closed when the source is processed.
 *
 * @since 1.0
 */
public record BEParser(BEType type, Scanner scanner) implements AutoCloseable {

    static final char EOF = 'e';
    static final char INTEGER_PREFIX = 'i';
    static final char LIST_PREFIX = 'l';
    static final char MAP_PREFIX = 'd';


    /**
     * Create a parser for the provided bencoded document.
     *
     * @param bs Bencoded document.
     * @since 1.0
     */
    public static BEParser create(byte[] bs) {

        if (bs == null || bs.length == 0) {
            throw new IllegalArgumentException("Can't parse bytes array: null or empty");
        }
        Scanner scanner = new Scanner(bs);
        BEType type = getTypeForPrefix((char) scanner.peek());
        return new BEParser(type, scanner);
    }

    static char getPrefixForType(BEType type) {

        if (type == null) {
            throw new NullPointerException("Can't get prefix -- type is null");
        }

        return switch (type) {
            case INTEGER -> INTEGER_PREFIX;
            case LIST -> LIST_PREFIX;
            case MAP -> MAP_PREFIX;
            default ->
                    throw new IllegalArgumentException("Unknown type: " + type.name().toLowerCase());
        };
    }

    static BEType getTypeForPrefix(char c) {
        if (Character.isDigit(c)) {
            return BEType.STRING;
        }
        switch (c) {
            case INTEGER_PREFIX -> {
                return BEType.INTEGER;
            }
            case LIST_PREFIX -> {
                return BEType.LIST;
            }
            case MAP_PREFIX -> {
                return BEType.MAP;
            }
            default -> throw new IllegalStateException("Invalid type prefix: " + c);
        }
    }

    static BEObjectBuilder<? extends BEObject> builderForType(BEType type) {
        switch (type) {
            case STRING -> {
                return new BEStringBuilder();
            }
            case INTEGER -> {
                return new BEIntegerBuilder();
            }
            case LIST -> {
                return new BEListBuilder();
            }
            case MAP -> {
                return new BEMapBuilder();
            }
            default ->
                    throw new IllegalStateException("Unknown type: " + type.name().toLowerCase());
        }
    }

    /**
     * Read type of the root object of the bencoded document that this parser was created for.
     *
     * @since 1.0
     */
    public BEType readType() {
        return type;
    }

    /**
     * Try to read the document's root object as a bencoded dictionary.
     *
     * @see #readType()
     * @since 1.0
     */
    public BEMap readMap() {
        return readMapObject(new BEMapBuilder());
    }


    private BEMap readMapObject(BEMapBuilder builderClass) {

        if (this.type != BEType.MAP) {
            throw new IllegalStateException(
                    "Can't read " + BEType.MAP.name().toLowerCase() +
                            " from: " + this.type.name().toLowerCase());
        }

        try {
            // relying on the default constructor being present
            return scanner.readMapObject(builderClass);
        } catch (Exception e) {
            throw new BtParseException(e);
        }
    }


    @Override
    public void close() {
        if (scanner != null) {
            scanner.close();
        }
    }
}
