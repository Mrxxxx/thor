package threads.magnet.bencoding;

/**
 * BEncoded document parse exception.
 *
 * @since 1.0
 */
class BtParseException extends RuntimeException {

    /**
     * @since 1.0
     */
    BtParseException(Throwable cause) {
        super("Failed to read from encoded data", cause);
    }

}
