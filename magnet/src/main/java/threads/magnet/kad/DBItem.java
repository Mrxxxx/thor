package threads.magnet.kad;

import static threads.magnet.utils.ArraysUtils.compareUnsigned;


public interface DBItem extends Comparable<DBItem> {

    byte[] item();

    // sort by raw data. only really useful for binary search
    default int compareTo(DBItem other) {
        return compareUnsigned(item(), other.item());
    }
}
