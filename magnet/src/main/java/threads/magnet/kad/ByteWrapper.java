package threads.magnet.kad;

import java.util.Arrays;

public record ByteWrapper(byte[] arr, int hash) {


    public static ByteWrapper createByteWrapper(byte[] a) {
        return new ByteWrapper(a, Arrays.hashCode(a));
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ByteWrapper && Arrays.equals(arr, ((ByteWrapper) obj).arr);
    }
}
