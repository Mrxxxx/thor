package threads.magnet.metainfo;

import androidx.annotation.NonNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public record TorrentFile(long size, Set<Integer> pieces, List<String> pathElements) {

    public static TorrentFile createTorrentFile(long size, List<String> pathElements) {

        if (size < 0) {
            throw new RuntimeException("Invalid torrent file size: " + size);
        }

        if (pathElements == null || pathElements.isEmpty()) {
            throw new RuntimeException("Can't create threads.torrent file without path");
        }

        return new TorrentFile(size, new HashSet<>(), pathElements);
    }

    @NonNull
    @Override
    public String toString() {
        return "TorrentFile{" +
                "size=" + size +
                ", pathElements=" + pathElements +
                '}';
    }

    public long getSize() {
        return size;
    }


    public Set<Integer> getPieces() {
        return new HashSet<>(pieces);
    }

    void addPiece(@NonNull Integer piece) {
        pieces.add(piece);
    }

    public List<String> getPathElements() {
        return pathElements;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TorrentFile that = (TorrentFile) obj;
        return size == that.size && pathElements.equals(that.pathElements);

    }

    @Override
    public int hashCode() {
        int result = (int) (size ^ (size >>> 32));
        result = 31 * result + pathElements.hashCode();
        return result;
    }
}
