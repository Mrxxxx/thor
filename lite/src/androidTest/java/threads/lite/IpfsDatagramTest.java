package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Objects;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;


@RunWith(AndroidJUnit4.class)
public class IpfsDatagramTest {


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void datagram_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);


        Dummy dummy = Dummy.getInstance(context);

        PeerId host = ipfs.self();
        assertNotNull(host);
        Multiaddr multiaddr = Multiaddr.getLoopbackAddress(ipfs.self(), server.getPort());


        try (Session dummySession = dummy.createSession()) {

            Connection conn = dummySession.dial(multiaddr, Parameters.getDefault());
            Objects.requireNonNull(conn);

            Thread.sleep(1000);

            assertEquals(server.numServerConnections(), 1);

            String text = "Moin Moin";


            // now send a datagram [no way yet implemented to check if the
            // datagram has arrived on the server, just an error log LogUtils.error,
            // since no application is defined to handle it]
            conn.sendDatagram(text.getBytes());

            Thread.sleep(1000);
        }

        Thread.sleep(3000);
        assertEquals(server.numServerConnections(), 0);


    }

}
