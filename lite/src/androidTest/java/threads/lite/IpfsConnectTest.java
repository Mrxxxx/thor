package threads.lite;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Objects;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Session;
import threads.lite.quic.Connection;


@RunWith(AndroidJUnit4.class)
public class IpfsConnectTest {
    private static final String TAG = IpfsConnectTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void peer_connect() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = ipfs.createSession()) {

            String address = "/dnsaddr/bootstrap.libp2p.io/p2p/QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa";
            Multiaddr multiaddr = IPFS.decodeMultiaddr(address);
            assertNotNull(multiaddr.host());

            // multiaddress is just a fiction
            Connection connection = IPFS.dial(session, multiaddr, IPFS.getConnectionParameters());
            assertTrue(connection.isConnected());


            PeerInfo peerInfo = ipfs.getPeerInfo(connection);
            LogUtils.error(TAG, peerInfo.toString());

            connection.close();
            // check reservation

            Reservation reservation =
                    IPFS.reservation(Objects.requireNonNull(TestEnv.getServer()), multiaddr);

            LogUtils.error(TAG, reservation.toString());
            reservation.connection().close();


        }
    }

    // @Test(expected = TimeoutException.class)
    public void swarm_connect() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String address = "/ip4/139.178.68.146/udp/4001/quic-v1/p2p/" +
                    "12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR";

            Multiaddr multiaddr = IPFS.decodeMultiaddr(address);
            assertNotNull(multiaddr.inetSocketAddress());
            assertEquals(multiaddr.inetSocketAddress().getPort(), multiaddr.port());
            assertEquals(multiaddr.inetSocketAddress().getAddress(), multiaddr.inetAddress());

            assertEquals(multiaddr.peerId(),
                    IPFS.decodePeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR"));


            // multiaddress is just a fiction
            IPFS.dial(session, multiaddr, IPFS.getConnectionParameters());

        }
    }


    @Test
    public void test_ipv6() throws Exception {

        String a1 = "/ip6/2804:d41:432f:3f00:ccbd:8e0d:a023:376b/udp/4001/quic-v1/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";
        String a2 = "/ip6/2804:d41:432f:3f00:94b9:ba41:dfc7:af66/udp/4001/quic-v1/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";
        String a3 = "/ip6/2804:d41:432f:3f00:a5b1:7947:debe:c88b/udp/4001/quic-v1/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";
        String a4 = "/ip6/2804:d41:432f:3f00:250e:c623:3b25:ddc8/udp/4001/quic-v1/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";

        Multiaddr multiaddr1 = Multiaddr.create(a1);
        assertNotNull(multiaddr1);
        Multiaddr multiaddr2 = Multiaddr.create(a2);
        assertNotNull(multiaddr2);
        Multiaddr multiaddr3 = Multiaddr.create(a3);
        assertNotNull(multiaddr3);
        Multiaddr multiaddr4 = Multiaddr.create(a4);
        assertNotNull(multiaddr4);
    }

}
