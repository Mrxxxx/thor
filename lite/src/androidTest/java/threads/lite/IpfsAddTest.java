package threads.lite;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.google.protobuf.ByteString;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.cid.Cid;
import threads.lite.cid.Dir;
import threads.lite.core.Link;
import threads.lite.core.Progress;
import threads.lite.core.Reader;
import threads.lite.core.Session;
import threads.lite.core.TimeoutCancellable;

@RunWith(AndroidJUnit4.class)
public class IpfsAddTest {

    private static final String TAG = IpfsAddTest.class.getSimpleName();
    private static final Context context = ApplicationProvider.getApplicationContext();


    @NonNull
    public File createCacheFile() throws IOException {
        return File.createTempFile("temp", ".io.ipfs.cid", context.getCacheDir());
    }

    @Test(expected = Exception.class)
    public void add_and_remove() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession(cid -> false)) {
            String content = "Hallo dfsadf";
            Cid text = IPFS.storeText(session, content);
            assertNotNull(text);
            assertTrue(IPFS.hasBlock(session, text));
            IPFS.removeBlocks(session, text);
            assertFalse(IPFS.hasBlock(session, text));

            IPFS.getText(session, text, new TimeoutCancellable(10)); // closed exception expected
        }
    }

    @Test
    public void add_dir() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Dir dir = IPFS.createEmptyDirectory(session);
            assertNotNull(dir);
            assertTrue(IPFS.isDir(session, dir.cid(), new TimeoutCancellable(1)));

            String content = "Hallo";
            Cid text = IPFS.storeText(session, content);
            assertNotNull(text);
            assertFalse(IPFS.isDir(session, text, new TimeoutCancellable(1)));

            byte[] data = IPFS.getData(session, text, new TimeoutCancellable(1));
            assertEquals(content, new String(data));

            dir = IPFS.addLinkToDirectory(session, dir,
                    Link.create(text, "text.txt", data.length, Link.File));
            assertNotNull(dir);
            assertTrue(dir.size() > 0);

            boolean exists = IPFS.hasLink(session, dir.cid(), "text.txt", () -> false);
            assertTrue(exists);

            exists = IPFS.hasLink(session, dir.cid(), "text2.txt", () -> false);
            assertFalse(exists);

            List<Link> links = IPFS.links(session, dir.cid(), false,
                    new TimeoutCancellable(1));
            assertNotNull(links);
            assertEquals(links.size(), 1);

            dir = IPFS.removeFromDirectory(session, dir, "text.txt");
            assertNotNull(dir);

            links = IPFS.links(session, dir.cid(), false, new TimeoutCancellable(1));
            assertNotNull(links);
            assertEquals(links.size(), 0);
        }
    }

    @Test
    public void update_dir() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Dir dir = IPFS.createEmptyDirectory(session);
            assertNotNull(dir);
            assertTrue(IPFS.isDir(session, dir.cid(), new TimeoutCancellable(1)));
            assertEquals(dir.size(), 0);

            String fileName = "test.txt";

            // TEST 1
            String content = "Hallo";
            Cid text = IPFS.storeText(session, content);
            assertNotNull(text);

            dir = IPFS.addLinkToDirectory(session, dir,
                    Link.create(text, fileName, content.length(), Link.File));
            assertNotNull(dir);
            assertEquals(dir.size(), content.length());

            List<Link> links = IPFS.links(session, dir.cid(), false, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 1);
            assertEquals(links.get(0).cid(), text);


            // TEST 2
            String contentNew = "Hallo Moin";
            Cid textNew = IPFS.storeText(session, content);
            assertNotNull(textNew);

            dir = IPFS.updateLinkToDirectory(session, dir,
                    Link.create(textNew, fileName, contentNew.length(), Link.File));
            assertNotNull(dir);
            assertEquals(dir.size(), contentNew.length());

            links = IPFS.links(session, dir.cid(), false, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 1);
            assertEquals(links.get(0).cid(), textNew);
        }
    }

    @Test
    public void create_dir() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {

            String content1 = "Hallo 1";
            Cid text1 = IPFS.storeText(session, content1);
            assertNotNull(text1);
            assertFalse(IPFS.isDir(session, text1, new TimeoutCancellable(1)));

            String content2 = "Hallo 12";
            Cid text2 = IPFS.storeText(session, content2);
            assertNotNull(text2);
            assertFalse(IPFS.isDir(session, text2, new TimeoutCancellable(1)));

            Dir dir = IPFS.createDirectory(session,
                    List.of(
                            Link.create(text1, "b.txt", content1.length(), Link.File),
                            Link.create(text2, "a.txt", content2.length(), Link.File))
            );

            assertNotNull(dir);
            assertTrue(IPFS.isDir(session, dir.cid(), new TimeoutCancellable(1)));

            List<Link> links = IPFS.links(session, dir.cid(), false,
                    new TimeoutCancellable(1));
            assertNotNull(links);
            assertEquals(links.size(), 2);

            assertEquals(links.get(0).cid().toString(), text1.toString());
            assertEquals(links.get(1).cid().toString(), text2.toString());
        }
    }

    @Test
    public void add_wrap_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession(cid -> false)) {
            int packetSize = 1000;
            long maxData = 1000;
            File inputFile = createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            LogUtils.debug(TAG, "Bytes : " + inputFile.length() / 1000 + "[kb]");

            Cid hash58Base = IPFS.storeFile(session, inputFile);
            assertNotNull(hash58Base);

            List<Link> links = IPFS.allLinks(session, hash58Base, true, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 4);

            byte[] bytes = IPFS.getData(session, hash58Base, () -> false);
            assertNotNull(bytes);
            assertEquals(bytes.length, size);


        }
    }

    @Test
    public void add_dir_test() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {

            File inputFile = new File(context.getCacheDir(), UUID.randomUUID().toString());
            assertTrue(inputFile.createNewFile());
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < 10; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(1000);
                    outputStream.write(randomBytes);
                }
            }

            Cid hash58Base = IPFS.storeFile(session, inputFile);
            assertNotNull(hash58Base);

            List<Link> links = IPFS.links(session, hash58Base, true, () -> false);
            assertNotNull(links);


            assertEquals(links.size(), 0);
        }
    }


    @Test
    public void add_test() throws Exception {

        int packetSize = 1000;
        long maxData = 1000;
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            File inputFile = createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            LogUtils.debug(TAG, "Bytes : " + inputFile.length() / 1000 + "[kb]");

            Cid hash58Base = IPFS.storeFile(session, inputFile);
            assertNotNull(hash58Base);

            List<Link> links = IPFS.allLinks(session, hash58Base, true, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 4);
            Link link = links.get(0);
            assertNotEquals(link.cid(), hash58Base);
            assertFalse(link.isDirectory());
            assertFalse(link.isFile());
            assertFalse(link.isUnknown());
            assertTrue(link.isRaw());

            byte[] bytes = IPFS.getData(session, hash58Base, () -> false);
            assertNotNull(bytes);
            assertEquals(bytes.length, size);

        }
    }


    @Test
    public void add_wrap_small_test() throws Exception {

        int packetSize = 200;
        long maxData = 1000;
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            File inputFile = createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();


            LogUtils.debug(TAG, "Bytes : " + inputFile.length() / 1000 + "[kb]");

            Cid hash58Base = IPFS.storeFile(session, inputFile);
            assertNotNull(hash58Base);

            List<Link> links = IPFS.links(session, hash58Base, true, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 0);

            byte[] bytes = IPFS.getData(session, hash58Base, () -> false);
            assertNotNull(bytes);
            assertEquals(bytes.length, size);
        }

    }

    @Test
    public void add_small_test() throws Exception {

        int packetSize = 200;
        long maxData = 1000;
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            File inputFile = createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            LogUtils.debug(TAG, "Bytes : " + inputFile.length() / 1000 + "[kb]");

            Cid hash58Base = IPFS.storeFile(session, inputFile);
            assertNotNull(hash58Base);

            List<Link> links = IPFS.links(session, hash58Base, true, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 0);

            byte[] bytes = IPFS.getData(session, hash58Base, () -> false);
            assertNotNull(bytes);
            assertEquals(bytes.length, size);

        }
    }


    @Test
    public void test_inputStream() throws Exception {


        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String text = "moin zehn";
            Cid cid = IPFS.storeText(session, text);
            assertTrue(IPFS.hasBlock(session, cid));

            byte[] bytes = IPFS.getData(session, cid, () -> false);
            assertNotNull(bytes);
            assertEquals(bytes.length, text.length());

            InputStream stream = IPFS.getInputStream(session, cid, () -> false);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            IPFS.copy(stream, outputStream);
            assertEquals(text, outputStream.toString());
        }
    }


    @Test
    public void test_inputStreamBig() throws Exception {


        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            byte[] text = TestEnv.getRandomBytes((IPFS.CHUNK_SIZE * 2) - 50);
            Cid cid = IPFS.storeData(session, text);

            byte[] bytes = IPFS.getData(session, cid, () -> false);
            assertNotNull(bytes);
            assertEquals(bytes.length, text.length);

            AtomicInteger percent = new AtomicInteger(0);
            //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
            InputStream stream = IPFS.getInputStream(session, cid, new Progress() {
                @Override
                public void setProgress(int progress) {
                    percent.set(progress);
                }

                @Override
                public boolean isCancelled() {
                    return false;
                }
            });


            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            IPFS.copy(stream, outputStream);
            assertArrayEquals(text, outputStream.toByteArray());
            assertEquals(100, percent.get());
        }
    }

    @Test
    public void test_reader() throws Exception {


        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String text = "0123456789 jjjjjjjj";
            Cid cid = IPFS.storeText(session, text);
            assertTrue(IPFS.hasBlock(session, cid));

            Reader reader = IPFS.getReader(session, cid, () -> false);
            reader.seek(0);
            ByteString buffer = reader.loadNextData();
            assertNotNull(buffer);
            assertEquals(text, new String(buffer.toByteArray()));

            int pos = 11;
            reader.seek(pos);
            buffer = reader.loadNextData();
            assertNotNull(buffer);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());
            assertEquals(text.substring(pos), stream.toString());

            pos = 5;
            reader.seek(pos);
            buffer = reader.loadNextData();
            assertNotNull(buffer);
            stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());
            assertEquals(text.substring(pos), stream.toString());
        }
    }

    @Test
    public void test_readerBig() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            byte[] text = TestEnv.getRandomBytes((IPFS.CHUNK_SIZE * 2) - 50);
            Cid cid = IPFS.storeData(session, text);
            assertTrue(IPFS.hasBlock(session, cid));

            Reader reader = IPFS.getReader(session, cid, () -> false);
            reader.seek(0);
            ByteString buffer = reader.loadNextData();
            assertNotNull(buffer);
            assertEquals(IPFS.CHUNK_SIZE, buffer.size());
            buffer = reader.loadNextData();
            assertNotNull(buffer);
            assertEquals(IPFS.CHUNK_SIZE - 50, buffer.size());

            int pos = IPFS.CHUNK_SIZE + 50;
            reader.seek(pos);
            buffer = reader.loadNextData();
            assertNotNull(buffer);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());

            assertEquals(IPFS.CHUNK_SIZE - 100, stream.size());

            pos = IPFS.CHUNK_SIZE - 50;
            reader.seek(pos);
            buffer = reader.loadNextData();
            assertNotNull(buffer);
            stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());
            assertEquals(50, stream.size());
        }
    }
}
