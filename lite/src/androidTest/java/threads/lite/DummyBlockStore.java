package threads.lite;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.core.BlockStore;

public class DummyBlockStore implements BlockStore {
    @Override
    public boolean hasBlock(@NonNull Cid cid) {
        return false;
    }

    @Nullable
    @Override
    public Block getBlock(@NonNull Cid cid) {
        return null;
    }

    @Nullable
    @Override
    public byte[] getBlockData(@NonNull Cid cid) {
        return null;
    }


    @Override
    public void deleteBlock(@NonNull Cid cid) {

    }

    @Override
    public void storeBlock(@NonNull Block block) {

    }

    @Override
    public void clear() {

    }
}
