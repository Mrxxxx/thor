package threads.lite.dag;

import androidx.annotation.NonNull;

import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;

import merkledag.pb.Merkledag;

public record DagVisitor(@NonNull Stack<DagStage> stack, @NonNull AtomicBoolean rootVisited) {

    public static DagVisitor createDagVisitor(@NonNull Merkledag.PBNode root) {
        DagVisitor dagVisitor = new DagVisitor(new Stack<>(), new AtomicBoolean(false));
        dagVisitor.pushActiveNode(root);
        return dagVisitor;
    }

    public void reset(@NonNull Stack<DagStage> dagStages) {
        stack.clear();
        stack.addAll(dagStages);
        rootVisited.set(false);
    }

    public void pushActiveNode(@NonNull Merkledag.PBNode node) {
        stack.push(DagStage.createDagStage(node));
    }

    public void popStage() {
        stack.pop();
    }

    public DagStage peekStage() {
        return stack.peek();
    }

    public boolean isRootVisited(boolean visited) {
        return rootVisited.getAndSet(visited);
    }

    public boolean isPresent() {
        return !stack.isEmpty();
    }

    @NonNull
    @Override
    public String toString() {
        String result = "";
        for (DagStage dagStage : stack) {
            result = result.concat(dagStage.toString());
        }
        return result;
    }
}
