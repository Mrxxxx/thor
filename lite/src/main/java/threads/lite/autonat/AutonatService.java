package threads.lite.autonat;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.DataHandler;
import threads.lite.core.PeerInfo;
import threads.lite.core.Session;
import threads.lite.ident.IdentityService;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamRequester;

public interface AutonatService {

    String TAG = AutonatService.class.getSimpleName();

    static void autonat(Session session, Set<Multiaddr> addresses,
                        Autonat autonat, Collection<Multiaddr> multiaddrs,
                        int timeout) {

        // TODO [Future urgent] replace by virtual threads
        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());


        Set<Connection> connections = getAutonatConnections(
                session, autonat, multiaddrs, timeout);
        if (!autonat.abort()) {

            for (Connection connection : connections) {

                service.execute(() -> {
                    try {
                        if (!autonat.abort()) {
                            autonat.addAddr(AutonatService.autonat(
                                    connection, session.self(), addresses));
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable.getClass().getSimpleName() + " " +
                                throwable.getMessage());
                    }
                });
            }
        }
        service.shutdown();
        try {
            boolean termination = service.awaitTermination(timeout, TimeUnit.SECONDS);
            if (!termination) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }

    @NonNull
    static Set<Connection> getAutonatConnections(Session session, Autonat autonat,
                                                 Collection<Multiaddr> multiaddrs, int timeout) {

        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());

        Set<Connection> connections = ConcurrentHashMap.newKeySet();

        for (Multiaddr multiaddr : multiaddrs) {

            service.execute(() -> {
                try {
                    if (!autonat.abort()) {
                        Connection connection = session.connect(multiaddr, Parameters.getDefault());

                        PeerInfo peerInfo = IdentityService.getPeerInfo(session.self(), connection);

                        if (peerInfo.hasProtocol(IPFS.AUTONAT_PROTOCOL)) {
                            Multiaddr observed = peerInfo.observed();
                            if (observed != null) {
                                autonat.evaluateNatType(observed, connection.localAddress());
                            }
                            connections.add(connection);
                        }
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable.getMessage());
                }
            });
        }
        service.shutdown();
        try {
            boolean termination = service.awaitTermination(timeout, TimeUnit.SECONDS);
            if (!termination) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
        return connections;

    }

    static Multiaddr autonat(Connection connection, PeerId self, Set<Multiaddr> addresses) throws Exception {

        autonat.pb.Autonat.Message message = AutonatService.dial(connection, self, addresses);

        if (message.hasDialResponse()) {
            autonat.pb.Autonat.Message.DialResponse dialResponse =
                    message.getDialResponse();
            if (dialResponse.hasStatusText()) {
                LogUtils.error(TAG, "Autonat Dial Text : "
                        + dialResponse.getStatusText());
            }
            if (dialResponse.hasStatus()) {
                if (dialResponse.getStatus() == autonat.pb.Autonat.Message.ResponseStatus.OK) {
                    ByteString raw = dialResponse.getAddr();
                    return Objects.requireNonNull(Multiaddr.create(self, raw.toByteArray()));

                } else {
                    throw new Exception(dialResponse.getStatusText());
                }
            } else {
                throw new Exception("invalid status");
            }
        }
        throw new Exception("invalid response");
    }

    @NonNull
    private static autonat.pb.Autonat.Message dial(
            Connection connection, PeerId peerId, Set<Multiaddr> addresses) throws Exception {

        CompletableFuture<autonat.pb.Autonat.Message> done = new CompletableFuture<>();
        autonat.pb.Autonat.Message.PeerInfo.Builder peerInfoBuilder =
                autonat.pb.Autonat.Message.PeerInfo.newBuilder();

        peerInfoBuilder.setId(ByteString.copyFrom(peerId.encoded()));


        for (Multiaddr addr : addresses) {
            peerInfoBuilder.addAddrs(ByteString.copyFrom(addr.address()));
        }

        autonat.pb.Autonat.Message.Dial dial = autonat.pb.Autonat.Message.Dial.newBuilder()
                .setPeer(peerInfoBuilder.build()).build();

        autonat.pb.Autonat.Message message = autonat.pb.Autonat.Message.newBuilder().
                setType(autonat.pb.Autonat.Message.MessageType.DIAL).
                setDial(dial).build();


        StreamRequester.createStream(connection, new AutonatRequest(done))
                .request(DataHandler.encode(message, IPFS.MULTISTREAM_PROTOCOL,
                        IPFS.AUTONAT_PROTOCOL), IPFS.DEFAULT_REQUEST_TIMEOUT);

        return done.get(IPFS.AUTONAT_TIMEOUT, TimeUnit.SECONDS);

    }

    record AutonatRequest(
            CompletableFuture<autonat.pb.Autonat.Message> done) implements StreamRequester {
        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void protocol(Stream stream, String protocol) throws Exception {
            if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.AUTONAT_PROTOCOL).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            done.complete(autonat.pb.Autonat.Message.parseFrom(data));
        }

        @Override
        public void fin(Stream stream) {
            // this is invoked, that is good, but request is finished when message is parsed [ok]
            LogUtils.error(TAG, "fin received " + stream.connection().remotePeerId());
        }

    }
}
