package threads.lite.core;

import android.net.DnsResolver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;
import threads.lite.dns.DnsData;
import threads.lite.dns.DnsMessage;
import threads.lite.dns.DnsName;
import threads.lite.dns.DnsQuestion;
import threads.lite.dns.DnsRecord;


public interface Resolver {
    String DNS_ADDR = "dnsaddr=";
    String DNS_LINK = "dnslink=";
    String TAG = Resolver.class.getSimpleName();
    ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    Random RANDOM = new Random();
    int UDP_PAYLOAD_SIZE = 1024;

    @NonNull
    static String resolveDnsLink(@NonNull String host) {

        Set<String> txtRecords = retrieveTxtRecords("_dnslink.".concat(host));
        for (String txtRecord : txtRecords) {
            if (txtRecord.startsWith(DNS_LINK)) {
                return txtRecord.replaceFirst(DNS_LINK, "");
            }
        }
        return "";
    }


    @NonNull
    static Set<Multiaddr> resolveDnsaddr(@NonNull IPV ipv, @NonNull Multiaddr multiaddr) {
        Set<Multiaddr> multiaddrs = new HashSet<>();
        if (!multiaddr.isDnsaddr()) {
            return multiaddrs;
        }
        String host = multiaddr.dnsHost();
        Objects.requireNonNull(host);

        try {
            PeerId peerId = multiaddr.peerId();
            Set<Multiaddr> addresses = resolveDnsaddrHost(ipv, host);
            for (Multiaddr addr : addresses) {
                PeerId cmpPeerId = addr.peerId();
                if (Objects.equals(cmpPeerId, peerId)) {
                    multiaddrs.add(addr);
                }
            }
        } catch (Throwable ignore) {
        }
        return multiaddrs;
    }


    @NonNull
    static Set<Multiaddr> resolveDnsaddrHost(@NonNull IPV ipv,
                                             @NonNull String host) {
        return resolveDnsAddressInternal(ipv, host, new HashSet<>());
    }

    @NonNull
    private static Set<Multiaddr> resolveDnsAddressInternal(@NonNull IPV ipv,
                                                            @NonNull String host,
                                                            @NonNull Set<String> hosts) {
        Set<Multiaddr> multiAddresses = new HashSet<>();
        // recursion protection
        if (hosts.contains(host)) {
            return multiAddresses;
        }
        hosts.add(host);

        Set<String> txtRecords = retrieveTxtRecords("_dnsaddr." + host);

        for (String txtRecord : txtRecords) {
            try {
                if (txtRecord.startsWith(DNS_ADDR)) {
                    String testRecordReduced = txtRecord.replaceFirst(DNS_ADDR, "");
                    Multiaddr multiaddr = Multiaddr.create(testRecordReduced);
                    if (multiaddr.isDnsaddr()) {
                        String childHost = multiaddr.dnsHost();
                        multiAddresses.addAll(resolveDnsAddressInternal(ipv, childHost, hosts));
                    } else {
                        if (multiaddr.protocolSupported(ipv)) {
                            multiAddresses.add(multiaddr);
                        }
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.warning(TAG, "Not supported " + txtRecord);
            }
        }
        return multiAddresses;
    }


    @NonNull
    private static Set<String> retrieveTxtRecords(@NonNull String host) {
        Set<String> txtRecords = ConcurrentHashMap.newKeySet();
        long start = System.currentTimeMillis();
        CountDownLatch finishFirst = new CountDownLatch(1);

        DnsQuestion question = DnsQuestion.create(DnsName.from(host));
        DnsMessage query = buildMessage(question).build();
        //noinspection AnonymousInnerClass
        DnsResolver.getInstance().rawQuery(null, query.serialize(),
                DnsResolver.FLAG_EMPTY, EXECUTOR_SERVICE, null,
                new DnsResolver.Callback<>() {
                    @Override
                    public void onAnswer(@NonNull byte[] bytes, int i) {
                        try {
                            DnsMessage dnsMessage = DnsMessage.parse(bytes);
                            for (DnsRecord dnsRecord : dnsMessage.answerSection()) {
                                DnsData payload = dnsRecord.getPayload();
                                if (payload instanceof DnsData.TXT text) {
                                    txtRecords.add(text.getText());
                                } else {
                                    LogUtils.error(TAG, host + " " + payload.toString());
                                }
                            }

                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, host + " " + throwable.getMessage());
                        } finally {
                            finishFirst.countDown();
                        }
                    }

                    @Override
                    public void onError(@NonNull DnsResolver.DnsException dnsException) {
                        LogUtils.error(TAG, host + " " + dnsException);
                        finishFirst.countDown();
                    }
                });


        try {
            boolean finished = finishFirst.await(IPFS.DEFAULT_REQUEST_TIMEOUT, TimeUnit.SECONDS);
            if (LogUtils.isDebug()) {
                LogUtils.error(TAG, "finished " + finished + " " +
                        (System.currentTimeMillis() - start) + "[ms]");
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable.getClass().getSimpleName()); // is ok, when it is InterruptedException
        }
        return txtRecords;
    }

    private static DnsMessage.Builder buildMessage(DnsQuestion question) {
        DnsMessage.Builder message = DnsMessage.builder();
        message.setQuestion(question);
        message.setId(RANDOM.nextInt());
        message.setRecursionDesired();
        message.getEdnsBuilder().setUdpPayloadSize(UDP_PAYLOAD_SIZE).setDnssecOk(false);
        return message;
    }

    @Nullable
    private static Multiaddr reachable(Multiaddrs multiaddrs) {
        if (multiaddrs.isEmpty()) {
            return null;
        }
        // the first one will not be checked if it is reachable [hole punching]
        if (multiaddrs.size() == 1) {
            return multiaddrs.first();
        }

        for (Multiaddr resolvedAddr : multiaddrs) {
            if (!resolvedAddr.isDnsaddr()) {
                if (isReachable(resolvedAddr)) { // only reachable
                    return resolvedAddr;
                }
            } else {
                return resolvedAddr;
            }
        }
        return null;
    }

    private static boolean isReachable(Multiaddr multiaddr) {
        try {
            return multiaddr.inetAddress().isReachable(IPFS.REACHABLE_TIMEOUT);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, multiaddr.toString() + " " + throwable.getMessage());
        }
        return false;
    }

    @Nullable
    static Multiaddr reduceToOne(Multiaddrs multiaddrs) {
        if (multiaddrs.isEmpty()) {
            return null;
        }
        return reachable(multiaddrs);
    }

    @Nullable
    static Multiaddr resolveToOne(@NonNull Supplier<IPV> ipv, @NonNull Multiaddr multiaddr) {
        Multiaddrs resolvedAddrs = new Multiaddrs();

        resolvedAddrs.addAll(resolveAddress(ipv, multiaddr));

        return reachable(resolvedAddrs);
    }

    // this includes circuit addresses
    static Multiaddrs reduce(@NonNull Supplier<IPV> ipv,
                             @NonNull PeerId peerId, @NonNull List<ByteString> byteStrings) {
        Multiaddrs multiaddrs = Multiaddr.create(peerId, byteStrings);
        Multiaddrs result = new Multiaddrs();
        for (Multiaddr multiaddr : multiaddrs) {
            if (multiaddr.protocolSupported(ipv.get())) {
                if (isReachable(multiaddr)) { // only reachable
                    result.add(multiaddr);
                }
            }
        }
        return result;
    }

    // this ignores circuit addresses
    @Nullable
    static Multiaddr reduceToOne(@NonNull Supplier<IPV> ipv,
                                 @NonNull PeerId peerId,
                                 @NonNull List<ByteString> byteStrings) {
        Multiaddrs multiaddrs = Multiaddr.create(peerId, byteStrings, ipv.get());
        return reachable(multiaddrs);
    }

    @NonNull
    private static Set<Multiaddr> resolveAddress(@NonNull Supplier<IPV> ipv,
                                                 @NonNull Multiaddr multiaddr) {
        if (multiaddr.isDnsaddr()) {
            return Resolver.resolveDnsaddr(ipv.get(), multiaddr);
        } else {
            return Set.of(multiaddr);
        }
    }
}
