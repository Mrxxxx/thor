package threads.lite.core;

import androidx.annotation.NonNull;

import threads.lite.cid.Block;
import threads.lite.cid.Cid;

public interface Swap {

    @NonNull
    Block getBlock(Cancellable cancellable, Cid cid) throws Exception;

    void close();
}
