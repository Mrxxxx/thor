package threads.lite.core;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.quic.Stream;

public final class MultistreamHandler implements ProtocolHandler {

    @Override
    public void protocol(Stream stream) {
        if (!stream.isInitiator()) { // TODO [Future high] make it independent of it
            stream.writeOutput(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL), false);
        }
    }

    @Override
    public void data(String alpn, Stream stream, byte[] data) throws Exception {
        throw new Exception("should not be invoked");
    }

    @Override
    public void fin(Stream stream) {
        LogUtils.error(getClass().getSimpleName(), "fin received" +
                stream.connection().remotePeerId());
    }

}
