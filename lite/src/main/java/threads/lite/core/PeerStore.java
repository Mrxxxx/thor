package threads.lite.core;

import androidx.annotation.NonNull;

import java.util.List;

import threads.lite.cid.Peer;

public interface PeerStore {

    List<Peer> getRandomPeers(int limit);

    void storePeer(@NonNull Peer peer);

    void removePeer(Peer peer);
}
