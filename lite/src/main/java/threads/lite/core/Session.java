package threads.lite.core;

import androidx.annotation.NonNull;

import java.io.Closeable;
import java.net.ConnectException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.host.LiteCertificate;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;


public interface Session extends Closeable, Routing, BitSwap, Host, Swarm {

    String TAG = Session.class.getSimpleName();

    // returns the block store where all data is stored
    @NonNull
    BlockStore getBlockStore();

    // returns true when session is closed, otherwise false (note: when session is closed
    // operations on a session will fail [Interrupt Exception]
    boolean isClosed();

    @NonNull
    Connection dial(Multiaddr address, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException;

    @NonNull
    default Connection dial(Cancellable cancellable, PeerId peerId, Parameters parameters)
            throws InterruptedException {


        Connection connection = getConnection(peerId);
        if (connection != null) {
            return connection;
        }

        AtomicReference<Connection> connectionAtomicReference =
                new AtomicReference<>(null);
        AtomicBoolean abort = new AtomicBoolean(false);
        findPeer(() -> cancellable.isCancelled() || abort.get(),
                multiaddr -> {
                    try {
                        // step by step connecting (though not fast)
                        synchronized (multiaddr.peerId().toString().intern()) {
                            if (cancellable.isCancelled()) {
                                return;
                            }
                            connectionAtomicReference.set(dial(multiaddr, parameters));
                            abort.set(true);
                        }
                        // abort other stuff
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }, peerId);

        if (cancellable.isCancelled()) {
            throw new InterruptedException("connection cancelled");
        }

        return connectionAtomicReference.get();
    }

    @NonNull
    LiteCertificate getLiteCertificate();

    @NonNull
    Function<Cid, Boolean> isBitswapActive();
}

