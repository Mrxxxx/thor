package threads.lite.quic;

public record StreamData(Stream stream, byte[] data, boolean isFinal, boolean isTerminated) {
}
