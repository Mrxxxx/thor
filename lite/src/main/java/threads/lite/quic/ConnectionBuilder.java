package threads.lite.quic;

import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.Resolver;
import threads.lite.host.LiteCertificate;
import threads.lite.host.LiteTrust;
import threads.lite.tls.CipherSuite;

public final class ConnectionBuilder {

    private static final String TAG = ConnectionBuilder.class.getSimpleName();
    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);


    @NonNull
    public static Connection connect(@NonNull StreamResponder streamResponder,
                                     @NonNull Multiaddr address,
                                     @NonNull Parameters parameters,
                                     @NonNull LiteCertificate liteCertificate,
                                     @NonNull Supplier<IPV> ipv)
            throws ConnectException, InterruptedException, TimeoutException {

        long start = System.currentTimeMillis();
        boolean run = false;

        DatagramSocket socket;
        try {
            socket = new DatagramSocket();
        } catch (SocketException socketException) {
            throw new ConnectException(socketException.getMessage());
        }

        try {
            ClientConnection connection = ConnectionBuilder.getConnection(streamResponder, address,
                    parameters, liteCertificate, ipv, socket);
            run = true;
            return connection;
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.debug(TAG, " Success " + run +
                        " (" + success.get() +
                        "," + failure.get() +
                        ") " + " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    @NonNull
    public static Connection connect(@NonNull StreamResponder streamResponder,
                                     @NonNull Multiaddr address,
                                     @NonNull Parameters parameters,
                                     @NonNull LiteCertificate liteCertificate,
                                     @NonNull Supplier<IPV> ipv,
                                     @NonNull DatagramSocket datagramSocket)
            throws ConnectException, InterruptedException, TimeoutException {

        long start = System.currentTimeMillis();
        boolean run = false;

        try {
            ClientConnection connection = ConnectionBuilder.getConnection(streamResponder,
                    address, parameters, liteCertificate, ipv, datagramSocket);
            run = true;
            return connection;
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.debug(TAG, " Success " + run +
                        " (" + success.get() +
                        "," + failure.get() +
                        ") " + " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    @NonNull
    private static ClientConnection getConnection(@NonNull StreamResponder streamResponder,
                                                  @NonNull Multiaddr address,
                                                  @NonNull Parameters parameters,
                                                  @NonNull LiteCertificate liteCertificate,
                                                  @NonNull Supplier<IPV> ipv,
                                                  @NonNull DatagramSocket datagramSocket)
            throws ConnectException, InterruptedException, TimeoutException {

        String serverName = address.host();
        Multiaddr resolved = address;
        if (resolved.isDnsaddr()) {
            resolved = Resolver.resolveToOne(ipv, address);
            if (resolved == null) {
                throw new ConnectException("no addresses left");
            }
        }


        InetSocketAddress inetSocketAddress;
        try {
            inetSocketAddress = resolved.inetSocketAddress();
        } catch (UnknownHostException unknownHostException) {
            throw new ConnectException(unknownHostException.getMessage());
        }
        Objects.requireNonNull(inetSocketAddress); // not possible

        int initialRtt = Settings.INITIAL_RTT;
        if (Network.isLocalAddress(inetSocketAddress.getAddress())) {
            initialRtt = 100;
        }


        Function<Stream, Consumer<StreamData>> streamDataConsumer = null;
        if (Objects.equals(parameters.alpn(), Settings.LIBP2P_ALPN)) {
            streamDataConsumer = quicStream -> AlpnLibp2pResponder.create(streamResponder);
        }
        if (Objects.equals(parameters.alpn(), Settings.LITE_ALPN)) {
            streamDataConsumer = quicStream -> AlpnLiteResponder.create(streamResponder);
        }
        Objects.requireNonNull(streamDataConsumer, "Alpn not handled");

        return ClientConnection.connect(
                parameters.alpn(), serverName, resolved.peerId(), inetSocketAddress,
                Version.QUIC_version_1, parameters.initialMaxData(), initialRtt,
                List.of(CipherSuite.TLS_AES_128_GCM_SHA256),
                new LiteTrust(resolved.peerId()), liteCertificate.x509Certificate(), liteCertificate.key(),
                datagramSocket, datagram -> LogUtils.error(TAG, Arrays.toString(datagram)),
                streamDataConsumer,
                parameters.keepAlive(), IPFS.CONNECT_TIMEOUT);

    }
}
