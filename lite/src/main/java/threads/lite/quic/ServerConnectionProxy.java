package threads.lite.quic;

import java.nio.ByteBuffer;


interface ServerConnectionProxy {


    void parsePackets(long timeReceived, ByteBuffer data);


    void terminate();
}
