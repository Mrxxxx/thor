package threads.lite.quic;

public record Parameters(String alpn, boolean keepAlive, int initialMaxData) {

    private static Parameters create(boolean keepAlive) {
        return new Parameters(Settings.LIBP2P_ALPN, keepAlive, Settings.INITIAL_MAX_DATA);
    }

    public static Parameters create(String alpn, boolean keepAlive) {
        return new Parameters(alpn, keepAlive, Settings.INITIAL_MAX_DATA);
    }

    public static Parameters getDefault() {
        return Parameters.create(false);
    }

    public static Parameters getDefault(boolean keepAlive) {
        return Parameters.create(keepAlive);
    }


}
