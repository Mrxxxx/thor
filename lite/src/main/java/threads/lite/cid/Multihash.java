package threads.lite.cid;


import androidx.annotation.NonNull;

import com.google.protobuf.CodedInputStream;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

import threads.lite.core.DataHandler;

public record Multihash(Hashtype type, byte[] hash) {

    private static final int MAX_IDENTITY_HASH_LENGTH = 1024 * 1024;

    public static Multihash create(Hashtype type, byte[] hash) {
        if (hash.length > 127 && type != Hashtype.ID)
            throw new IllegalStateException("Unsupported hash size: " + hash.length);
        if (hash.length > MAX_IDENTITY_HASH_LENGTH)
            throw new IllegalStateException("Unsupported hash size: " + hash.length);
        if (hash.length != type.length() && type != Hashtype.ID)
            throw new IllegalStateException("Incorrect hash length: " + hash.length + " != " + type.length());
        return new Multihash(type, hash);
    }

    @NonNull
    public static Multihash decode(CodedInputStream codedInputStream) {
        try {
            int type = codedInputStream.readRawVarint32();
            int len = codedInputStream.readRawVarint32();
            Hashtype t = Hashtype.lookup(type);
            byte[] hash = codedInputStream.readRawBytes(len);
            if (!codedInputStream.isAtEnd()) {
                throw new IllegalStateException("still data available");
            }
            return create(t, hash);
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    public static Multihash decode(byte[] raw) {
        return decode(CodedInputStream.newInstance(raw));
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, Arrays.hashCode(hash)); // ok, checked, maybe opt
    }

    @NonNull
    public byte[] encoded() {

        int typeLength = DataHandler.unsignedVariantSize(type.index());
        int hashLength = DataHandler.unsignedVariantSize(hash.length);

        ByteBuffer buffer = ByteBuffer.allocate(typeLength + hashLength + hash.length);
        DataHandler.writeUnsignedVariant(buffer, type.index());
        DataHandler.writeUnsignedVariant(buffer, hash.length);
        buffer.put(hash);
        return buffer.array();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Multihash))
            return false;
        return type == ((Multihash) o).type && Arrays.equals(hash, ((Multihash) o).hash);
    }
}