/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package threads.lite.tls;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

// https://tools.ietf.org/html/rfc8446#section-4.4.3
// "Certificate Verify
//   This message is used to provide explicit proof that an endpoint possesses the private key corresponding to its certificate.  The
//   CertificateVerify message also provides integrity for the handshake up to this point.  Servers MUST send this message when authenticating
//   via a certificate.  Clients MUST send this message whenever authenticating via a certificate (i.e., when the Certificate message
//   is non-empty). "
public record CertificateVerifyMessage(SignatureScheme signatureScheme, byte[] signature,
                                       byte[] raw) implements HandshakeMessage {

    private static final int MINIMUM_MESSAGE_SIZE = 1 + 3 + 2 + 2 + 1;

    static CertificateVerifyMessage createCertificateVerifyMessage(SignatureScheme signatureScheme, byte[] signature) {

        int signatureLength = signature.length;
        ByteBuffer buffer = ByteBuffer.allocate(4 + 2 + 2 + signatureLength);
        buffer.putInt((HandshakeType.certificate_verify.value << 24) | (2 + 2 + signatureLength));
        buffer.putShort(signatureScheme.value);
        buffer.putShort((short) signatureLength);
        buffer.put(signature);
        return new CertificateVerifyMessage(signatureScheme, signature, buffer.array());
    }

    public static CertificateVerifyMessage parse(ByteBuffer buffer, int length) throws ErrorAlert {
        int startPosition = buffer.position();
        int remainingLength = HandshakeMessage.parseHandshakeHeader(buffer, HandshakeType.certificate_verify, MINIMUM_MESSAGE_SIZE);

        try {
            short signatureSchemeValue = buffer.getShort();
            SignatureScheme signatureScheme = SignatureScheme.get(signatureSchemeValue);

            int signatureLength = buffer.getShort() & 0xffff;
            byte[] signature = new byte[signatureLength];
            buffer.get(signature);
            if (buffer.position() - startPosition != 4 + remainingLength) {
                throw new DecodeErrorException("Incorrect message length");
            }

            byte[] raw = new byte[length];
            buffer.position(startPosition);
            buffer.get(raw);

            return new CertificateVerifyMessage(signatureScheme, signature, raw);
        } catch (BufferUnderflowException notEnoughBytes) {
            throw new DecodeErrorException("message underflow");
        }
    }

    @Override
    public HandshakeType getType() {
        return HandshakeType.certificate_verify;
    }

    @Override
    public byte[] getBytes() {
        return raw;
    }

}
