/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package threads.lite.tls;

import androidx.annotation.NonNull;

import java.util.Date;


public record NewSessionTicket(byte[] psk, long ticketCreation,
                               long ticketAgeAdd, byte[] ticket,
                               int ticketLifeTime, boolean hasEarlyDataExtension,
                               long earlyDataMaxSize) {


    public static NewSessionTicket create(TlsState state, NewSessionTicketMessage newSessionTicketMessage) throws BadRecordMacAlert {
        boolean hasEarlyDataExtension = newSessionTicketMessage.earlyDataExtension() != null;
        long earlyDataMaxSize = 0;
        if (hasEarlyDataExtension) {
            earlyDataMaxSize = newSessionTicketMessage.earlyDataExtension().maxEarlyDataSize();
        }
        return new NewSessionTicket(state.computePSK(newSessionTicketMessage.ticketNonce()),
                System.currentTimeMillis(), newSessionTicketMessage.ticketAgeAdd(),
                newSessionTicketMessage.ticket(), newSessionTicketMessage.ticketLifetime(),
                hasEarlyDataExtension, earlyDataMaxSize);
    }


    private int validFor() {
        return Integer.max(0, (int) ((ticketCreation + ticketLifeTime * 1000) - new Date().getTime()) / 1000);
    }

    public boolean hasEarlyDataExtension() {
        return hasEarlyDataExtension;
    }

    public long getEarlyDataMaxSize() {
        return earlyDataMaxSize;
    }

    @NonNull
    @Override
    public String toString() {
        return "Ticket, creation date = " + ticketCreation + ", ticket lifetime = " + ticketLifeTime
                + (validFor() > 0 ? " (still valid for " + validFor() + " seconds)" : " (not valid anymore)");
    }
}
