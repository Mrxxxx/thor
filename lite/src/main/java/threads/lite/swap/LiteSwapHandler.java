package threads.lite.swap;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Objects;

import swap.pb.Swap;
import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.DataHandler;
import threads.lite.core.ProtocolHandler;
import threads.lite.host.LiteHost;
import threads.lite.quic.Settings;
import threads.lite.quic.Stream;

public final class LiteSwapHandler implements ProtocolHandler {

    private final LiteHost host;

    public LiteSwapHandler(@NonNull LiteHost host) {
        this.host = host;
    }

    @Override
    public void protocol(Stream stream) {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.LITE_SWAP_PROTOCOL), false);
    }

    @Override
    public void data(String alpn, Stream stream, byte[] data) throws Exception {
        Swap.SwapRequest request = Swap.SwapRequest.parseFrom(data);
        Cid cid = Cid.decode(request.getBlock().toByteArray());
        byte[] blockData = host.getBlockStore().getBlockData(cid);
        if (blockData != null) {
            Swap.SwapResponse.Block srb = Swap.SwapResponse.Block.newBuilder()
                    .setData(ByteString.copyFrom(blockData))
                    .setPrefix(ByteString.copyFrom(cid.getPrefix().encoded())).build();
            Swap.SwapResponse response =
                    Swap.SwapResponse.newBuilder()
                            .setStatus(Swap.SwapResponse.Status.Have)
                            .setBlock(srb).build();
            if (Objects.equals(alpn, Settings.LITE_ALPN)) {
                stream.response(response);
            } else {
                stream.writeOutput(DataHandler.encode(response), true);
            }
        } else {
            Swap.SwapResponse response = Swap.SwapResponse.newBuilder()
                    .setStatus(Swap.SwapResponse.Status.DontHave).build();
            if (Objects.equals(alpn, Settings.LITE_ALPN)) {
                stream.response(response);
            } else {
                stream.writeOutput(DataHandler.encode(response), true);
            }
        }
    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
    }

}
