package threads.lite.swap;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import swap.pb.Swap;
import threads.lite.IPFS;
import threads.lite.core.DataHandler;
import threads.lite.quic.Connection;
import threads.lite.quic.Settings;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamRequester;

public interface LiteSwapService {

    @NonNull
    static Swap.SwapResponse swap(@NonNull Connection connection,
                                  @NonNull Swap.SwapRequest request) throws Exception {
        if (Objects.equals(connection.alpn(), Settings.LITE_ALPN)) {
            // shortcut
            return Swap.SwapResponse.parseFrom(StreamRequester.createStream(connection)
                    .request(DataHandler.createMessage(IPFS.LITE_SWAP_PROTOCOL, request),
                            IPFS.GRACE_PERIOD));
        } else {
            CompletableFuture<Swap.SwapResponse> done = new CompletableFuture<>();

            StreamRequester.createStream(connection, new SwapRequester(done))
                    .request(DataHandler.encode(request, IPFS.MULTISTREAM_PROTOCOL,
                            IPFS.LITE_SWAP_PROTOCOL), IPFS.GRACE_PERIOD);

            return done.get(IPFS.GRACE_PERIOD, TimeUnit.SECONDS);
        }
    }

    record SwapRequester(CompletableFuture<Swap.SwapResponse> done) implements StreamRequester {
        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void protocol(Stream stream, String protocol) throws Exception {
            if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                    IPFS.LITE_SWAP_PROTOCOL).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            done.complete(Swap.SwapResponse.parseFrom(data));
        }

        @Override
        public void fin(Stream stream) {
            // this is invoked, that is good, but request is finished when message is parsed [ok]
        }


        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

    }


}
