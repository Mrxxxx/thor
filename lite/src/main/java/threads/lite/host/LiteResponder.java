package threads.lite.host;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.DataHandler;
import threads.lite.core.ProtocolHandler;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamResponder;


public record LiteResponder(
        @NonNull Map<String, ProtocolHandler> protocols) implements StreamResponder {
    private static final String TAG = LiteResponder.class.getSimpleName();

    @Nullable
    private ProtocolHandler getProtocolHandler(@NonNull String protocol) {
        return protocols.get(protocol);
    }

    @Override
    public void throwable(Throwable throwable) {
        LogUtils.error(TAG, throwable);
    }


    @Override
    public void protocol(Stream stream, String protocol) throws Exception {
        ProtocolHandler protocolHandler = getProtocolHandler(protocol);
        if (protocolHandler != null) {
            protocolHandler.protocol(stream);
        } else {
            LogUtils.error(TAG, "Ignore " + protocol + " " + stream.connection().remotePeerId());
            stream.writeOutput(DataHandler.encodeProtocols(IPFS.NA), false); // TODO [Future low] maybe closing ?
        }
    }

    @Override
    public void data(Stream stream, String protocol, byte[] data) throws Exception {
        Objects.requireNonNull(protocol, "data unknown protocol "
                + stream.connection().remotePeerId());
        ProtocolHandler protocolHandler = getProtocolHandler(protocol);
        if (protocolHandler != null) {
            protocolHandler.data(stream.alpn(), stream, data);
        } else {
            LogUtils.error(TAG, "data unknown protocol " + stream.connection().remotePeerId());
        }

    }


    @NonNull
    Set<String> getProtocolNames() {
        return protocols.keySet();
    }

    @Override
    public void fin(Stream stream, String protocol) {
        Objects.requireNonNull(protocol, "data unknown protocol "
                + stream.connection().remotePeerId());
        ProtocolHandler protocolHandler = getProtocolHandler(protocol);
        if (protocolHandler != null) {
            protocolHandler.fin(stream);
        } else {
            LogUtils.error(TAG, "fin unknown protocol " +
                    stream.connection().remotePeerId());
        }

    }

}


