package threads.lite.mdns;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Server;
import threads.lite.quic.Settings;

public class MDNS {
    @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
    private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();
    private static final String[] EMPTY = new String[0];
    private static final String TAG = MDNS.class.getName();
    private final NsdManager nsdManager;
    private final String mdnsName;
    private volatile DiscoveryService discoveryService;
    private volatile boolean serviceStarted = false;

    private MDNS(NsdManager nsdManager, String mdnsName) {
        this.nsdManager = nsdManager;
        this.mdnsName = mdnsName;
    }

    public static MDNS create(Context context, String mdnsName) {
        NsdManager nsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
        Objects.requireNonNull(nsdManager);
        return new MDNS(nsdManager, mdnsName);
    }

    public void startService(Server server) {
        PeerId peerId = server.self();
        String ownServiceName = peerId.toString();


        NsdServiceInfo serviceInfo = new NsdServiceInfo();

        serviceInfo.setAttribute(IPFS.PROTOCOLS,
                String.join(";", server.getProtocolNames()));
        serviceInfo.setAttribute(IPFS.ALPNS, Settings.LITE_ALPN + ";" + Settings.LIBP2P_ALPN);

        serviceInfo.setServiceName(ownServiceName);
        serviceInfo.setServiceType(mdnsName);
        serviceInfo.setPort(server.getPort());

        nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD,
                RegistrationService.getInstance());
        serviceStarted = true;
    }

    public void startDiscovery(Consumer<Peer> consumer) {
        try {
            this.discoveryService = new DiscoveryService(nsdManager, consumer);

            nsdManager.discoverServices(mdnsName,
                    NsdManager.PROTOCOL_DNS_SD, discoveryService);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void stop() {
        try {
            if (serviceStarted) {
                nsdManager.unregisterService(RegistrationService.getInstance());
                serviceStarted = false;
            }
            if (discoveryService != null) {
                nsdManager.stopServiceDiscovery(discoveryService);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable); // not ok when fails
        }
    }


    private record DiscoveryService(@NonNull NsdManager nsdManager,
                                    @NonNull Consumer<Peer> consumer)
            implements NsdManager.DiscoveryListener {
        private static final String TAG = DiscoveryService.class.getSimpleName();


        private static String[] evaluateProtocols(
                @NonNull NsdServiceInfo serviceInfo) {
            try {
                Map<String, byte[]> attributes = serviceInfo.getAttributes();
                byte[] bytes = attributes.get(IPFS.PROTOCOLS);
                if (bytes != null && bytes.length > 0) {
                    String protocols = new String(bytes);
                    return protocols.split(";");
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable); // not ok when fails
            }
            return EMPTY;
        }

        private static String[] evaluateAlpns(
                @NonNull NsdServiceInfo serviceInfo) {
            try {
                Map<String, byte[]> attributes = serviceInfo.getAttributes();
                byte[] bytes = attributes.get(IPFS.ALPNS);
                if (bytes != null && bytes.length > 0) {
                    String alpns = new String(bytes);
                    return alpns.split(";");
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage()); // not ok when fails
            }
            return EMPTY;
        }

        @Override
        public void onStartDiscoveryFailed(String serviceType, int errorCode) {
            LogUtils.debug(TAG, "onStartDiscoveryFailed");
        }

        @Override
        public void onStopDiscoveryFailed(String serviceType, int errorCode) {
            LogUtils.debug(TAG, "onStopDiscoveryFailed");
        }

        @Override
        public void onDiscoveryStarted(String serviceType) {
            LogUtils.debug(TAG, "onDiscoveryStarted");
        }

        @Override
        public void onDiscoveryStopped(String serviceType) {
            LogUtils.debug(TAG, "onDiscoveryStopped");
        }

        @Override
        public void onServiceFound(NsdServiceInfo serviceInfo) {


            if (Build.VERSION.SDK_INT >= 34) {
                //noinspection AnonymousInnerClass
                nsdManager.registerServiceInfoCallback(serviceInfo, EXECUTOR,
                        new NsdManager.ServiceInfoCallback() {
                            @Override
                            public void onServiceInfoCallbackRegistrationFailed(int i) {

                            }

                            @Override
                            public void onServiceUpdated(@NonNull NsdServiceInfo nsdServiceInfo) {
                                evaluateHosts(nsdServiceInfo);
                            }

                            @Override
                            public void onServiceLost() {
                            }

                            @Override
                            public void onServiceInfoCallbackUnregistered() {

                            }
                        });
            } else {
                //noinspection AnonymousInnerClass
                nsdManager.resolveService(serviceInfo, new NsdManager.ResolveListener() {

                    @Override
                    public void onResolveFailed(NsdServiceInfo nsdServiceInfo, int i) {
                        LogUtils.debug(TAG, "onResolveFailed " + nsdServiceInfo.toString());
                    }

                    @Override
                    public void onServiceResolved(NsdServiceInfo nsdServiceInfo) {
                        evaluateHost(nsdServiceInfo);
                    }
                });
            }
        }

        @Override
        public void onServiceLost(NsdServiceInfo serviceInfo) {
            LogUtils.info(TAG, "onServiceLost " + serviceInfo.toString());
        }

        private void evaluateHost(@NonNull NsdServiceInfo serviceInfo) {
            try {
                PeerId decodedPeerId = PeerId.decode(serviceInfo.getServiceName());

                String[] protocols = evaluateProtocols(serviceInfo);
                String[] alpns = evaluateAlpns(serviceInfo);

                InetAddress inetAddress = serviceInfo.getHost();

                Multiaddr multiaddr = Multiaddr.create(decodedPeerId,
                        new InetSocketAddress(inetAddress, serviceInfo.getPort()));
                consumer.accept(new Peer(multiaddr, protocols, alpns));
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage()); // is ok, fails for kubo
            }
        }

        @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
        private void evaluateHosts(@NonNull NsdServiceInfo serviceInfo) {
            try {
                PeerId decodedPeerId = PeerId.decode(serviceInfo.getServiceName());
                String[] protocols = evaluateProtocols(serviceInfo);
                String[] alpns = evaluateAlpns(serviceInfo);

                for (InetAddress inetAddress : serviceInfo.getHostAddresses()) {

                    Multiaddr multiaddr = Multiaddr.create(decodedPeerId,
                            new InetSocketAddress(inetAddress, serviceInfo.getPort()));
                    consumer.accept(new Peer(multiaddr, protocols, alpns));
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage()); // is ok, fails for kubo
            }
        }

    }

    public record Peer(Multiaddr multiaddr, String[] protocols, String[] alpns) {
    }

    public static class RegistrationService implements NsdManager.RegistrationListener {
        private static final String TAG = RegistrationService.class.getSimpleName();

        public static RegistrationService getInstance() {
            return InstanceHolder.INSTANCE;
        }

        @Override
        public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            LogUtils.info(TAG, "RegistrationFailed : " + errorCode);
        }

        @Override
        public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            LogUtils.info(TAG, "Un-RegistrationFailed : " + errorCode);
        }

        @Override
        public void onServiceRegistered(NsdServiceInfo serviceInfo) {
            LogUtils.info(TAG, "ServiceRegistered : " + serviceInfo.getServiceName());
        }

        @Override
        public void onServiceUnregistered(NsdServiceInfo serviceInfo) {
            LogUtils.info(TAG, "Un-ServiceRegistered : " + serviceInfo.getServiceName());
        }

        private static final class InstanceHolder {
            private static final RegistrationService INSTANCE = new RegistrationService();
        }
    }

}
