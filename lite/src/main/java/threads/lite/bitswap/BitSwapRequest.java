package threads.lite.bitswap;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamRequester;
import threads.lite.quic.TransportError;

public record BitSwapRequest(CompletableFuture<Void> done) implements StreamRequester {
    @Override
    public void throwable(Throwable throwable) {
        done.completeExceptionally(throwable);
    }

    @Override
    public void protocol(Stream stream, String protocol) throws Exception {
        if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                IPFS.BITSWAP_PROTOCOL).contains(protocol)) {
            throw new Exception("Token " + protocol + " not supported");
        }

        if (Objects.equals(protocol, IPFS.BITSWAP_PROTOCOL)) {
            done.complete(null); // this is not 100% correct [a fin would be much nicer, sometimes it is returned]
            stream.closeInput(TransportError.Code.NO_ERROR);
        }
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        throw new Exception("data method invoked [not expected]");
    }

    @Override
    public void terminated() {
        if (!done.isDone()) {
            done.completeExceptionally(new Throwable("stream terminated"));
        }
    }


    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
        LogUtils.error(getClass().getSimpleName(), "fin received " +
                stream.connection().remotePeerId());
    }

}
