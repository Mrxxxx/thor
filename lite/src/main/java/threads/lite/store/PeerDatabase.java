package threads.lite.store;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.lite.cid.ID;
import threads.lite.cid.Peer;

@androidx.room.Database(entities = {Peer.class}, version = 2, exportSchema = false)
@TypeConverters({Peer.class, ID.class})
public abstract class PeerDatabase extends RoomDatabase {

    public abstract PeerDao bootstrapDao();

}
