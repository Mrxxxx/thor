package threads.lite.cert;

import androidx.annotation.NonNull;

import java.io.IOException;

/**
 * ASN.1 IA5String object - this is a ISO 646 (ASCII) string encoding code points 0 to 127.
 * <p>
 * Explicit character set escape sequences are not allowed.
 * </p>
 */
public abstract class ASN1IA5String extends ASN1Primitive implements ASN1String {
    private final byte[] contents;

    ASN1IA5String(String string) {
        if (string == null) {
            throw new NullPointerException("'string' cannot be null");
        }
        if (!isIA5String(string)) {
            throw new IllegalArgumentException("'string' contains illegal characters");
        }

        this.contents = Strings.toByteArray(string);
    }

    ASN1IA5String(byte[] contents) {
        this.contents = contents;
    }

    /**
     * return true if the passed in String can be represented without
     * loss as an IA5String, false otherwise.
     *
     * @param str the string to check.
     * @return true if character set in IA5String set, false otherwise.
     */
    private static boolean isIA5String(String str) {
        for (int i = str.length() - 1; i >= 0; i--) {
            char ch = str.charAt(i);
            if (ch > 0x007f) {
                return false;
            }
        }

        return true;
    }

    static ASN1IA5String createPrimitive(byte[] contents) {
        return new DERIA5String(contents);
    }

    public final String getString() {
        return Strings.fromByteArray(contents);
    }

    @NonNull
    public String toString() {
        return getString();
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.IA5_STRING, contents);
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1IA5String that)) {
            return false;
        }

        return Arrays.areEqual(this.contents, that.contents);
    }

}
