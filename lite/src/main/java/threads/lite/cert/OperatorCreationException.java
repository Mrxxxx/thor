package threads.lite.cert;

class OperatorCreationException extends OperatorException {
    OperatorCreationException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
