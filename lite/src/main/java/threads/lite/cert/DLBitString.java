package threads.lite.cert;

import java.io.IOException;

/**
 * A Definite length BIT STRING
 */
final class DLBitString extends ASN1BitString {

    DLBitString(byte[] contents) {
        super(contents);
    }

    static int encodedLength(boolean withTag, int contentsLength) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contentsLength);
    }

    static void encode(ASN1OutputStream out, boolean withTag, byte[] buf, int len) throws IOException {
        out.writeEncodingDL(withTag, BERTags.BIT_STRING, buf, 0, len);
    }

    static void encode(ASN1OutputStream out, byte pad, byte[] buf, int off, int len)
            throws IOException {
        out.writeEncodingDL(pad, buf, off, len);
    }

    boolean encodeConstructed() {
        return false;
    }

    int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.BIT_STRING, contents);
    }

    ASN1Primitive toDLObject() {
        return this;
    }
}
