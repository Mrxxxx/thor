package threads.lite.cert;

import androidx.annotation.NonNull;

import java.io.IOException;

/**
 * - * UTC time object.
 * Internal facade of {@link ASN1UTCTime}.
 * <p>
 * This datatype is valid only from 1950-01-01 00:00:00 UTC until 2049-12-31 23:59:59 UTC.
 * </p>
 * <hr>
 * <p><b>X.690</b></p>
 * <p><b>11: Restrictions on BER employed by both CER and DER</b></p>
 * <p><b>11.8 UTCTime </b></p>
 * <b>11.8.1</b> The encoding shall terminate with "Z",
 * as described in the ITU-T X.680 | ISO/IEC 8824-1 clause on UTCTime.
 * <p>
 * <b>11.8.2</b> The seconds element shall always be present.
 * <p>
 * <b>11.8.3</b> Midnight (GMT) shall be represented in the form:
 * <blockquote>
 * "YYMMDD000000Z"
 * </blockquote>
 * where "YYMMDD" represents the day following the midnight in question.
 */
public class ASN1UTCTime extends ASN1Primitive {
    private final byte[] contents;

    /**
     * The correct format for this is YYMMDDHHMMSSZ (it used to be that seconds were
     * never encoded. When you're creating one of these objects from scratch, that's
     * what you want to use, otherwise we'll try to deal with whatever gets read from
     * the input stream... (this is why the input format is different from the getTime()
     * method output).
     * <p>
     *
     * @param time the time string.
     */
    ASN1UTCTime(String time) {
        this.contents = Strings.toByteArray(time);
    }

    private ASN1UTCTime(byte[] contents) {
        if (contents.length < 2) {
            throw new IllegalArgumentException("UTCTime string too short");
        }
        this.contents = contents;
        if (!(isDigit(0) && isDigit(1))) {
            throw new IllegalArgumentException("illegal characters in UTCTime string");
        }
    }

    static ASN1UTCTime createPrimitive(byte[] contents) {
        return new ASN1UTCTime(contents);
    }

    private boolean isDigit(int pos) {
        return contents.length > pos && contents[pos] >= '0' && contents[pos] <= '9';
    }

    final boolean encodeConstructed() {
        return false;
    }

    int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.UTC_TIME, contents);
    }

    boolean asn1Equals(ASN1Primitive o) {
        if (!(o instanceof ASN1UTCTime)) {
            return false;
        }
        return Arrays.areEqual(contents, ((ASN1UTCTime) o).contents);
    }

    @NonNull
    public String toString() {
        return Strings.fromByteArray(contents);
    }
}
