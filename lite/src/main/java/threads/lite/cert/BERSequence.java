package threads.lite.cert;

import java.io.IOException;

/**
 * Indefinite length SEQUENCE of objects.
 * <p>
 * Length field has value 0x80, and the sequence ends with two bytes of: 0x00, 0x00.
 * </p><p>
 * For X.690 syntax rules, see {@link ASN1Sequence}.
 * </p>
 */
final class BERSequence extends ASN1Sequence {

    /**
     * Create a sequence containing a vector of objects.
     */
    BERSequence(ASN1EncodableVector elementVector) {
        super(elementVector);
    }

    int encodedLength(boolean withTag) throws IOException {
        int totalLength = withTag ? 4 : 3;

        for (ASN1Encodable element : elements) {
            ASN1Primitive p = element.toASN1Primitive();
            totalLength += p.encodedLength(true);
        }

        return totalLength;
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingIL(withTag, BERTags.CONSTRUCTED | BERTags.SEQUENCE, elements);
    }


}
