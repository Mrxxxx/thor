package threads.lite.cert;

import androidx.annotation.NonNull;

/**
 * A NULL object - use DERNull.INSTANCE for populating structures.
 */
public abstract class ASN1Null extends ASN1Primitive {


    @SuppressWarnings("SameReturnValue")
    static ASN1Null createPrimitive(byte[] contents) {
        if (0 != contents.length) {
            throw new IllegalStateException("malformed NULL encoding encountered");
        }
        return DERNull.INSTANCE;
    }

    boolean asn1Equals(
            ASN1Primitive o) {
        return o instanceof ASN1Null;
    }

    @NonNull
    public String toString() {
        return "NULL";
    }
}
