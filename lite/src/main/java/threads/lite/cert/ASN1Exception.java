package threads.lite.cert;

import java.io.IOException;

/**
 * Exception thrown in cases of corrupted or unexpected data in a stream.
 */
class ASN1Exception extends IOException {

    /**
     * Base constructor
     *
     * @param message a message concerning the exception.
     */
    ASN1Exception(String message) {
        super(message);
    }


}
