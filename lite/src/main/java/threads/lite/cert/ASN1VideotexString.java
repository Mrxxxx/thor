package threads.lite.cert;

import java.io.IOException;

public abstract class ASN1VideotexString extends ASN1Primitive implements ASN1String {
    private final byte[] contents;

    /**
     * basic constructor - with bytes.
     */
    ASN1VideotexString(byte[] contents) {
        this.contents = contents;
    }

    static ASN1VideotexString createPrimitive(byte[] contents) {
        return new DERVideotexString(contents);
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.VIDEOTEX_STRING, contents);
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1VideotexString that)) {
            return false;
        }

        return Arrays.areEqual(this.contents, that.contents);
    }


    public final String getString() {
        return Strings.fromByteArray(contents);
    }
}
