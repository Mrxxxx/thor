package threads.lite.cert;

/**
 * class for breaking up an X500 Name into it's component tokens, ala
 * java.util.StringTokenizer. We need this class as some of the
 * lightweight Java environment don't support classes like
 * StringTokenizer.
 */
final class X500NameTokenizer {

    private final String value;
    private final char separator;
    private int index;

    X500NameTokenizer(String oid) {
        this(oid, ',');
    }

    X500NameTokenizer(String oid, char separator) {
        this.value = oid;
        this.index = -1;
        this.separator = separator;
    }

    boolean hasMoreTokens() {
        return (index != value.length());
    }

    String nextToken() {
        if (index == value.length()) {
            return null;
        }

        int end = index + 1;
        boolean quoted = false;
        boolean escaped = false;
        StringBuilder buf = new StringBuilder();
        buf.setLength(0);

        while (end != value.length()) {
            char c = value.charAt(end);

            if (c == '"') {
                if (!escaped) {
                    quoted = !quoted;
                }
                buf.append(c);
                escaped = false;
            } else {
                if (escaped || quoted) {
                    buf.append(c);
                    escaped = false;
                } else if (c == '\\') {
                    buf.append(c);
                    escaped = true;
                } else if (c == separator) {
                    break;
                } else {
                    buf.append(c);
                }
            }
            end++;
        }

        index = end;

        return buf.toString();
    }
}
