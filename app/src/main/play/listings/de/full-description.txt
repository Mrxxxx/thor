Der Thor Browser basiert auf der WebKit-API und ist ein Open-Source-Projekt.
Der Schwerpunkt der Anwendung liegt in der Unterstützung von IPFS Lite im Browser.
Es ist ein  Ad-Blocker integriert, der auf https://pgl.yoyo.org/adservers basiert.