Version 152
-------------
- API: Simplify MDNS and Swarm interface
- Feature: Introduce Request and Response message style starting with own protocols
- API: Update network has to be triggered from the outside (when network changed,
fixes a bug, that also a server should be triggered)
- API: Server publish in identity only public [dialable] addresses (before it also publish
circuit addresses when available)
- API: Own identity [IdentityHandler] of a client or server connection are now pre-stored [faster access]
- API: Identify [IdentityHandler] does not include the observed address anymore [faster generation and faster access]
- UI: tiny bugfixes
- Feature: Initial version of upnp [not yet active]
- Feature: better ALPN support for QUIC [plan to use ALPN different from libp2p for own protocols]
- MDNS: Support of protocols in the discovery service
- MDNS: Support of registration service
- MDNS: Kubo mdns is not supported anymore (due to bad kubo performance)
- API: fixes + coding style issues
- Android: MinSdk Version is now 29 (Android Q) This enables the use of a faster DNS Resolver,
 which is non blocking [though not yet used, but will be important for virtual threads]
- API: DnsResolver based on Android DnsResolver (MinSdk 29)