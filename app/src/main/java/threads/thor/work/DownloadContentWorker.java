package threads.thor.work;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.io.OutputStream;
import java.util.List;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.Link;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.core.DOCS;
import threads.thor.core.locals.LOCALS;
import threads.thor.utils.MimeTypeService;

public final class DownloadContentWorker extends Worker {

    private static final String TAG = DownloadContentWorker.class.getSimpleName();

    private static final String URI = "uri";
    private final AtomicBoolean success = new AtomicBoolean(true);

    /**
     * @noinspection WeakerAccess
     */
    public DownloadContentWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri content) {

        Data.Builder data = new Data.Builder();
        data.putString(URI, content.toString());

        return new OneTimeWorkRequest.Builder(DownloadContentWorker.class)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri content) {
        WorkManager.getInstance(context).enqueue(getWork(content));
    }

    @Nullable
    private static Uri downloadsUri(Context context, String mimeType, String name, String path) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.Images.ImageColumns.DISPLAY_NAME, name);
        contentValues.put(MediaStore.Images.ImageColumns.MIME_TYPE, mimeType);
        contentValues.put(MediaStore.Images.ImageColumns.RELATIVE_PATH, path);
        ContentResolver contentResolver = context.getContentResolver();
        return contentResolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues);
    }

    @NonNull
    @Override
    public Result doWork() {


        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... ");

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            LOCALS locals = LOCALS.getInstance();
            DOCS.IsBitswapActive isBitswapActive = new DOCS.IsBitswapActive();
            try (Session session = ipfs.createSession(isBitswapActive)) {

                // todo maybe optimize this
                locals.locals().parallelStream().forEach(multiaddr -> {
                    try {
                        if (!Objects.equals(multiaddr.peerId(), ipfs.self())) {
                            session.connect(multiaddr, IPFS.getConnectionParameters());
                        }
                    } catch (Throwable ignore) {
                    }
                });


                Uri uri = Uri.parse(getInputData().getString(URI));

                String name = DOCS.getFileName(uri);

                setForegroundAsync(createForegroundInfo(name, 0));

                DOCS.Content content = docs.getContent(session, uri, this::isStopped);
                Objects.requireNonNull(content);

                if (content.root().name() != null) {
                    isBitswapActive.deactivate();
                }


                boolean directory = IPFS.isDir(session, content.cid(), this::isStopped);
                Stack<String> paths = new Stack<>();
                paths.push(name);
                if (directory) {
                    downloadLinks(session, paths, content.cid());
                } else {
                    download(session, paths, content.cid());
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

    private void download(@NonNull Session session, @NonNull Stack<String> paths,
                          @NonNull Cid cid) throws Exception {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start [" + (System.currentTimeMillis() - start) + "]...");


        String name = paths.pop();
        Objects.requireNonNull(name);

        String mimeType = MimeTypeService.getMimeType(name);
        String path = Environment.DIRECTORY_DOWNLOADS + File.separator + String.join((File.separator), paths);

        Uri downloads = downloadsUri(getApplicationContext(), mimeType, name, path);
        Objects.requireNonNull(downloads);
        ContentResolver contentResolver = getApplicationContext().getContentResolver();

        if (!IPFS.isDir(session, cid, this::isStopped)) {
            try (OutputStream os = contentResolver.openOutputStream(downloads)) {
                Objects.requireNonNull(os, "Failed to open output stream.");
                IPFS.fetchToOutputStream(session, os, cid, new Progress() {
                    @Override
                    public boolean isCancelled() {
                        return isStopped();
                    }

                    @Override
                    public void setProgress(int progress) {
                        setForegroundAsync(createForegroundInfo(name, progress));
                    }
                });

            } catch (Throwable throwable) {
                success.set(false);
                contentResolver.delete(downloads, null, null);
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }
        }
    }

    private void evalLinks(@NonNull Session session, @NonNull Stack<String> paths,
                           @NonNull List<Link> links) throws Exception {

        for (Link link : links) {
            if (!isStopped()) {
                Cid cid = link.cid();
                paths.push(link.name());
                if (IPFS.isDir(session, cid, this::isStopped)) {
                    downloadLinks(session, paths, cid);
                } else {
                    download(session, paths, cid);
                }
            }
        }

    }


    private void downloadLinks(@NonNull Session session, @NonNull Stack<String> paths,
                               @NonNull Cid content) throws Exception {

        List<Link> links = IPFS.links(session, content, false, this::isStopped);

        evalLinks(session, paths, links);

    }

    @NonNull
    private ForegroundInfo createForegroundInfo(@NonNull String name, int progress) {

        Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                InitApplication.STORAGE_CHANNEL_ID);

        PendingIntent cancelPendingIntent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());
        String cancel = getApplicationContext().getString(android.R.string.cancel);

        Intent intent = InitApplication.getDownloadsIntent();

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Notification.Action action = new Notification.Action.Builder(
                Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                cancelPendingIntent).build();

        builder.setContentTitle(name)
                .setSubText(progress + "%")
                .setContentIntent(pendingIntent)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .addAction(action)
                .setCategory(Notification.CATEGORY_PROGRESS)
                .setUsesChronometer(true)
                .setOngoing(true);


        Notification notification = builder.build();

        int notificationId = getId().hashCode();
        return new ForegroundInfo(notificationId, notification);
    }
}
