package threads.thor.core.tabs;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import java.util.Objects;

@androidx.room.Entity
public record Tab(@PrimaryKey(autoGenerate = true) long idx,
                  @NonNull @ColumnInfo(name = "title") String title,
                  @NonNull @ColumnInfo(name = "uri") String uri,
                  @Nullable @ColumnInfo(name = "image", typeAffinity = ColumnInfo.BLOB) byte[] image) {

    @NonNull
    public static Tab createTab(@NonNull String title, @NonNull String uri,
                                @Nullable byte[] image) {
        return new Tab(0, title, uri, image);
    }


    @Override
    @Nullable
    public byte[] image() {
        return image;
    }


    @NonNull
    @Override
    public String toString() {
        return "Tab{" +
                "uri='" + uri + '\'' +
                ", title='" + title + '\'' +
                ", idx=" + idx +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tab tab = (Tab) o;
        return idx == tab.idx;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idx);
    }

    @Nullable
    public Bitmap bitmap() {
        if (image != null) {
            return BitmapFactory.decodeByteArray(image, 0, image.length);
        }
        return null;
    }

    @Override
    @NonNull
    public String uri() {
        return uri;
    }

    @Override
    @NonNull
    public String title() {
        return title;
    }

    @Override
    public long idx() {
        return idx;
    }


}
