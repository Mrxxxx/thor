package threads.thor.core.books;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import java.util.Objects;

/**
 * @param timestamp checked
 */
@androidx.room.Entity
public record Bookmark(@PrimaryKey @NonNull @ColumnInfo(name = "uri") String uri,
                       @NonNull @ColumnInfo(name = "title") String title,
                       @ColumnInfo(name = "timestamp") long timestamp,
                       @Nullable @ColumnInfo(typeAffinity = ColumnInfo.BLOB) byte[] icon,
                       @Nullable @ColumnInfo(name = "dnsLink") String dnsLink) {


    @Override
    @Nullable
    public String dnsLink() {
        return dnsLink;
    }

    @Override
    public long timestamp() {
        return timestamp;
    }

    @Override
    public byte[] icon() {
        return icon;
    }


    public boolean areItemsTheSame(@NonNull Bookmark bookmark) {
        return Objects.equals(uri, bookmark.uri);
    }

    @Nullable
    public Bitmap bitmap() {
        if (icon != null) {
            return BitmapFactory.decodeByteArray(icon, 0, icon.length);
        }
        return null;
    }

    @Override
    @NonNull
    public String uri() {
        return uri;
    }

    @Override
    @NonNull
    public String title() {
        return title;
    }

}
