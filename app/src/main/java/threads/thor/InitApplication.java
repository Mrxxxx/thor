package threads.thor;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.DownloadManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.util.ArrayMap;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;

import com.google.android.material.color.DynamicColors;

import threads.lite.IPFS;
import threads.lite.mdns.MDNS;
import threads.thor.core.events.EVENTS;
import threads.thor.core.locals.LOCALS;
import threads.thor.utils.AdBlocker;

public class InitApplication extends Application {


    public static final String STORAGE_CHANNEL_ID = "STORAGE_CHANNEL_ID";
    @NonNull
    private static final ArrayMap<String, String> SEARCH_ENGINES = new ArrayMap<>();
    private static final String TAG = InitApplication.class.getSimpleName();
    private static final String APP_KEY = "AppKey";
    private static final String JAVASCRIPT_KEY = "javascriptKey";
    private static final String REDIRECT_URL_KEY = "redirectUrlKey";
    private static final String REDIRECT_INDEX_KEY = "redirectIndexKey";
    private static final String SEARCH_ENGINE_KEY = "searchEngineKey";
    private volatile MDNS mdns;

    public static void setJavascriptEnabled(Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(JAVASCRIPT_KEY, auto);
        editor.apply();
    }

    public static boolean isJavascriptEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(JAVASCRIPT_KEY, true);

    }

    public static void setRedirectUrlEnabled(Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(REDIRECT_URL_KEY, auto);
        editor.apply();
    }

    public static boolean isRedirectUrlEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(REDIRECT_URL_KEY, false);
    }

    public static void setRedirectIndexEnabled(@NonNull Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(REDIRECT_INDEX_KEY, auto);
        editor.apply();
    }

    public static boolean isRedirectIndexEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(REDIRECT_INDEX_KEY, true);

    }

    public static String getSearchEngine(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(SEARCH_ENGINE_KEY, context.getString(R.string.duck_duck_go));
    }

    public static void setSearchEngine(@NonNull Context context, @NonNull String searchEngine) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SEARCH_ENGINE_KEY, searchEngine);
        editor.apply();
    }

    @NonNull
    public static Intent getDownloadsIntent() {
        return new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS);
    }

    @SuppressLint("SetJavaScriptEnabled")
    public static void setWebSettings(@NonNull WebView webView, boolean enableJavascript) {


        WebSettings settings = webView.getSettings();
        settings.setUserAgentString("Mozilla/5.0 (Linux; Android " + Build.VERSION.RELEASE + ")");


        settings.setJavaScriptEnabled(enableJavascript);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);

        settings.setSafeBrowsingEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(false);
        settings.setLoadsImagesAutomatically(true);
        settings.setBlockNetworkLoads(false);
        settings.setBlockNetworkImage(false);
        settings.setDomStorageEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setDatabaseEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        settings.setMixedContentMode(WebSettings.MIXED_CONTENT_NEVER_ALLOW);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setMediaPlaybackRequiresUserGesture(true);
        settings.setSupportMultipleWindows(false);
        settings.setGeolocationEnabled(false);
    }

    @NonNull
    public static ArrayMap<String, String> getSearchEngines() {
        return SEARCH_ENGINES;
    }

    @NonNull
    public static Engine getEngine(@NonNull Context context) {
        String searchEngine = InitApplication.getSearchEngine(context);
        String uri = SEARCH_ENGINES.get(searchEngine);
        if (uri == null) {
            return new Engine(context.getString(R.string.duck_duck_go),
                    Uri.parse("https://start.duckduckgo.com/"));
        }
        return new Engine(searchEngine, Uri.parse(uri));
    }

    private static void createStorageChannel(@NonNull Context context) {

        try {
            CharSequence name = context.getString(R.string.storage_channel_name);
            String description = context.getString(R.string.storage_channel_description);
            NotificationChannel mChannel = new NotificationChannel(
                    STORAGE_CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        if (mdns != null) {
            mdns.stop();
            mdns = null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        createStorageChannel(getApplicationContext());
        // adding search engines
        SEARCH_ENGINES.put(getString(R.string.petal_search), "https://www.petalsearch.com/");
        SEARCH_ENGINES.put(getString(R.string.google), "https://www.google.com/");
        SEARCH_ENGINES.put(getString(R.string.duck_duck_go), "https://start.duckduckgo.com/");

        // register service very fast

        LOCALS locals = LOCALS.getInstance();


        mdns = IPFS.mdns(InitApplication.this);
        mdns.startDiscovery(locals::addPeer);


        // always start with fresh public/private key
        IPFS.setPrivateKey(getApplicationContext(), "");
        IPFS.setPublicKey(getApplicationContext(), "");

        DynamicColors.applyToActivitiesIfAvailable(this);


        AdBlocker.init(getApplicationContext());

        EVENTS events = EVENTS.getInstance(getApplicationContext());

        try {
            IPFS.getInstance(getApplicationContext());
        } catch (Throwable throwable) {
            events.fatal(throwable.getClass().getSimpleName() +
                    " " + throwable.getMessage());
            if (mdns != null) {
                mdns.stop();
                mdns = null;
            }
            LogUtils.error(TAG, throwable);
        }

    }

    public record Engine(String name, Uri uri) {
    }
}