package threads.thor.fragments;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.ArrayMap;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.sidesheet.SideSheetDialog;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.IPFS;
import threads.lite.core.Cancellable;
import threads.lite.core.Session;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.MainActivity;
import threads.thor.R;
import threads.thor.core.DOCS;
import threads.thor.core.books.BOOKS;
import threads.thor.core.books.Bookmark;
import threads.thor.core.events.EVENTS;
import threads.thor.core.tabs.TABS;
import threads.thor.core.tabs.Tab;
import threads.thor.model.ThorModel;
import threads.thor.utils.AdBlocker;
import threads.thor.utils.CustomWebChromeClient;
import threads.thor.utils.HistoryAdapter;
import threads.thor.utils.MimeTypeService;
import threads.thor.work.BrowserResetWorker;
import threads.thor.work.DownloadContentWorker;
import threads.thor.work.DownloadFileWorker;
import threads.thor.work.DownloadMagnetWorker;

public class BrowserFragment extends Fragment {
    private static final String TAG = BrowserFragment.class.getSimpleName();
    private static final String ABOUT_SCHEME = "about";
    @NonNull
    private final Set<Uri> uris = ConcurrentHashMap.newKeySet();
    @NonNull
    private final ConcurrentHashMap<Integer, Session> sessions = new ConcurrentHashMap<>();
    private final ActivityResultLauncher<Intent> mFolderRequestForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();

                    try {
                        Objects.requireNonNull(data);
                        Uri uri = data.getData();
                        Objects.requireNonNull(uri);


                        String mimeType = requireActivity().getContentResolver().getType(uri);

                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(uri, mimeType);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                        startActivity(intent);

                    } catch (Throwable e) {
                        EVENTS.getInstance(requireContext()).warning(
                                getString(R.string.no_activity_found_to_handle_uri));
                    }
                }
            });
    private final AtomicReference<Bitmap> oldFavicon = new AtomicReference<>(null);
    private boolean hasCamera;
    private WebView webView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CircularProgressIndicator progressIndicator;
    private long lastClickTime;
    private WindowSizeClass widthWindowSizeClass = WindowSizeClass.COMPACT;
    private ActionMode actionMode;
    private long tabItem;
    private CoordinatorLayout root;
    private MaterialTextView tabTitle;
    private ShapeableImageView tabIcon;
    private MaterialButton nextPage;
    private MaterialButton previousPage;
    private final ActivityResultLauncher<ScanOptions>
            mScanRequestForResult = registerForActivityResult(new ScanContract(),
            result -> {
                if (result.getContents() != null) {
                    try {
                        Uri uri = Uri.parse(result.getContents());
                        if (uri != null) {
                            String scheme = uri.getScheme();
                            if (Objects.equals(scheme, DOCS.IPNS_SCHEME) ||
                                    Objects.equals(scheme, DOCS.IPFS_SCHEME) ||
                                    Objects.equals(scheme, DOCS.HTTP_SCHEME) ||
                                    Objects.equals(scheme, DOCS.HTTPS_SCHEME)) {
                                openUri(uri);
                            } else {
                                EVENTS.getInstance(requireContext()).error(
                                        getString(R.string.codec_not_supported));
                            }
                        } else {
                            EVENTS.getInstance(requireContext()).error(
                                    getString(R.string.codec_not_supported));
                        }
                    } catch (Throwable throwable) {
                        EVENTS.getInstance(requireContext()).error(
                                getString(R.string.codec_not_supported));
                    }
                }
            });
    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    invokeScan();
                } else {
                    EVENTS.getInstance(requireContext()).permission(
                            getString(R.string.permission_camera_denied));
                }
            });
    private MaterialButton reloadPage;
    private MaterialButton home;

    private static void tearDown(@NonNull PopupWindow dialog) {
        try {
            Thread.sleep(150);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            dialog.dismiss();
        }
    }

    private int numUris() {
        return uris.size();
    }

    private void detachUri(@NonNull Uri uri) {
        uris.remove(uri);
    }

    private void attachUri(@NonNull Uri uri) {
        uris.add(uri);
    }

    @NonNull
    private Session getSession(int authority) throws Exception {
        Session session = sessions.get(authority);
        if (session != null) {
            return session;
        }

        IPFS ipfs = IPFS.getInstance(requireContext());

        Session newSession = ipfs.createSession(new DOCS.IsBitswapActive());

        DOCS docs = DOCS.getInstance(requireContext());

        // todo maybe optimize this
        docs.locals().parallelStream().forEach(multiaddr -> {
            try {
                if (!Objects.equals(multiaddr.peerId(), ipfs.self())) {
                    IPFS.dial(newSession, multiaddr, IPFS.getConnectionParameters());
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
        sessions.put(authority, newSession);

        return newSession;
    }

    private boolean hasSession(int authority) {
        return sessions.containsKey(authority);
    }

    // release all sessions except the session with the given authority
    private void releaseSessions(int authority) {
        for (Map.Entry<Integer, Session> entry : sessions.entrySet()) {
            if (!Objects.equals(entry.getKey(), authority)) {
                Session session = sessions.remove(entry.getKey());
                if (session != null) {
                    session.close();
                }
            }
        }
    }

    private void cleanup() {
        for (Map.Entry<Integer, Session> entry : sessions.entrySet()) {
            Session session = sessions.remove(entry.getKey());
            if (session != null) {
                session.close();
            }
        }
        uris.clear();
        sessions.clear();
    }

    private void computeWindowSizeClasses() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;


        if (widthDp < 600f) {
            widthWindowSizeClass = WindowSizeClass.COMPACT;
        } else {
            widthWindowSizeClass = WindowSizeClass.MEDIUM;
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_browser, container, false);
    }

    public void updateTitle(String title, String url) {
        try {
            this.tabTitle.setText(title);
            TABS tabs = TABS.getInstance(requireContext());
            tabs.updateTab(tabItem, title, url);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void updateFavicon(@NonNull Bitmap icon) {
        try {
            if (!icon.sameAs(oldFavicon.get())) {
                oldFavicon.set(icon);
                tabIcon.startAnimation(
                        AnimationUtils.makeInAnimation(requireContext(), true));
                tabIcon.setImageBitmap(icon);
            }
            TABS tabs = TABS.getInstance(requireContext());
            tabs.updateTabIcon(tabItem, icon);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void showDocumentation() {
        try {
            String uri = "https://gitlab.com/remmer.wilts/thor";

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri),
                    requireContext(), MainActivity.class);
            startActivity(intent);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void showSettings() {
        try {
            Dialog settingsDialog = getDialog();

            settingsDialog.setContentView(R.layout.fragment_settings);


            SwitchMaterial enableJavascript = settingsDialog.findViewById(
                    R.id.enable_javascript);
            Objects.requireNonNull(enableJavascript);
            enableJavascript.setChecked(InitApplication.isJavascriptEnabled(requireContext()));
            enableJavascript.setOnCheckedChangeListener((buttonView, isChecked) ->
                    InitApplication.setJavascriptEnabled(requireContext(), isChecked)
            );


            SwitchMaterial enableRedirectUrl = settingsDialog.findViewById(
                    R.id.enable_redirect_url);
            Objects.requireNonNull(enableRedirectUrl);
            enableRedirectUrl.setChecked(InitApplication.isRedirectUrlEnabled(requireContext()));
            enableRedirectUrl.setOnCheckedChangeListener((buttonView, isChecked) ->
                    InitApplication.setRedirectUrlEnabled(requireContext(), isChecked)
            );

            SwitchMaterial enableRedirectIndex = settingsDialog.findViewById(
                    R.id.enable_redirect_index);
            Objects.requireNonNull(enableRedirectIndex);
            enableRedirectIndex.setChecked(InitApplication.isRedirectIndexEnabled(requireContext()));
            enableRedirectIndex.setOnCheckedChangeListener((buttonView, isChecked) ->
                    InitApplication.setRedirectIndexEnabled(requireContext(), isChecked)
            );


            ArrayMap<String, String> searchEngines = InitApplication.getSearchEngines();

            MaterialAutoCompleteTextView search_engine_menu =
                    settingsDialog.findViewById(R.id.search_engine_menu);


            //noinspection SimplifyStreamApiCallChains
            String[] items = searchEngines.keySet().stream().toArray(String[]::new);
            search_engine_menu.setSimpleItems(items);

            String searchEngine = InitApplication.getSearchEngine(requireContext());
            int pos = searchEngines.indexOfKey(searchEngine);

            search_engine_menu.setListSelection(pos);
            search_engine_menu.setText(searchEngine, false);


            search_engine_menu.setOnItemClickListener((parent, view, position, id) -> {
                try {
                    InitApplication.setSearchEngine(requireContext(),
                            searchEngines.keyAt(position));

                    search_engine_menu.dismissDropDown();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });


            settingsDialog.show();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            computeWindowSizeClasses();
            visibilityActions();
            updateActions();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void visibilityActions() {
        boolean expandedScreen = widthWindowSizeClass == WindowSizeClass.MEDIUM;

        if (expandedScreen) {
            previousPage.setVisibility(View.VISIBLE);
        } else {
            previousPage.setVisibility(View.GONE);
        }

        if (expandedScreen) {
            nextPage.setVisibility(View.VISIBLE);
        } else {
            nextPage.setVisibility(View.GONE);
        }

        if (expandedScreen) {
            reloadPage.setVisibility(View.VISIBLE);
        } else {
            reloadPage.setVisibility(View.GONE);
        }
    }

    private void updateActions() {
        previousPage.setEnabled(webView.canGoBack());
        nextPage.setEnabled(webView.canGoForward());
    }

    private void bookmark() {
        try {
            BOOKS books = BOOKS.getInstance(requireContext());
            String url = webView.getUrl();
            Objects.requireNonNull(url);
            Bookmark bookmark = books.getBookmark(url);
            if (bookmark != null) {
                String msg = bookmark.title();
                books.removeBookmark(bookmark);

                if (msg.isEmpty()) {
                    msg = url;
                }
                EVENTS.getInstance(requireContext()).warning(
                        getString(R.string.bookmark_removed, msg));
            } else {
                Bitmap bitmap = getFavicon();
                String title = getTitle();

                bookmark = BOOKS.createBookmark(url, title, bitmap);
                books.storeBookmark(bookmark);

                String msg = title;
                if (msg.isEmpty()) {
                    msg = url;
                }

                EVENTS events = EVENTS.getInstance(requireContext());
                events.warning(getString(R.string.bookmark_added, msg));
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private boolean hasBookmark() {
        try {
            BOOKS books = BOOKS.getInstance(requireContext());

            String url = webView.getUrl();
            if (url != null && !url.isEmpty()) {
                return books.hasBookmark(url);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        webView.onResume();
    }

    public boolean onBackPressedCheck() {

        if (webView.canGoBack()) {
            previousPage();
            return true;
        }
        return false;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        webView.saveState(outState);
    }

    private void clearWebView() {
        webView.clearHistory();
        webView.clearCache(true);
        webView.clearFormData();

    }

    private boolean downloadActive() {
        try {
            String url = webView.getUrl();
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), DOCS.IPFS_SCHEME) ||
                        Objects.equals(uri.getScheme(), DOCS.IPNS_SCHEME)) {
                    return true;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    private void contentDownloader(@NonNull Uri uri) {

        if (!isResumed()) {
            return;
        }

        NotificationManager notificationManager = (NotificationManager)
                requireContext().getSystemService(NOTIFICATION_SERVICE);

        if (!notificationManager.areNotificationsEnabled()) {
            EVENTS.getInstance(requireContext()).permission(
                    getString(R.string.notification_permission_required));
            return;
        }

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.download);
        builder.setMessage(DOCS.getFileName(uri));

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {
            DownloadContentWorker.download(requireContext(), uri);

            progressIndicator.setVisibility(View.INVISIBLE);
        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.INVISIBLE);
                    dialog.cancel();
                });
        builder.show();


    }

    private void magnetDownloader(@NonNull Uri uri) {

        if (!isResumed()) {
            return;
        }

        NotificationManager notificationManager = (NotificationManager)
                requireContext().getSystemService(NOTIFICATION_SERVICE);

        if (!notificationManager.areNotificationsEnabled()) {
            EVENTS.getInstance(requireContext()).permission(
                    getString(R.string.notification_permission_required));
            return;
        }

        MagnetUri magnetUri = MagnetUriParser.parse(uri.toString());

        String name = uri.toString();
        if (magnetUri.getDisplayName().isPresent()) {
            name = magnetUri.getDisplayName().get();
        }

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.download);
        builder.setMessage(name);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {

            DownloadMagnetWorker.download(requireContext(), uri);

            progressIndicator.setVisibility(View.INVISIBLE);
        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.INVISIBLE);
                    dialog.cancel();
                });
        builder.show();

    }

    private void fileDownloader(@NonNull Uri uri, @NonNull String filename,
                                @NonNull String mimeType, long size) {

        if (!isResumed()) {
            return;
        }
        NotificationManager notificationManager = (NotificationManager)
                requireContext().getSystemService(NOTIFICATION_SERVICE);

        if (!notificationManager.areNotificationsEnabled()) {
            EVENTS.getInstance(requireContext()).permission(
                    getString(R.string.notification_permission_required));
            return;
        }

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.download);
        builder.setMessage(filename);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {

            DownloadFileWorker.download(requireContext(), uri, filename, mimeType, size);

            progressIndicator.setVisibility(View.INVISIBLE);

        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.INVISIBLE);
                    dialog.cancel();
                });
        builder.show();
    }

    private void reloadPage() {

        try {
            if (oldFavicon.get() == null) {
                setFavicon();
            }
            progressIndicator.setVisibility(View.INVISIBLE);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        try {
            DOCS.cleanupResolver(Uri.parse(webView.getUrl()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        try {
            webView.reload();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void openUri(@NonNull Uri uri) {
        try {
            progressIndicator.setVisibility(View.INVISIBLE);

            oldFavicon.set(null);
            setFavicon();
            if (Objects.equals(uri.getScheme(), DOCS.IPNS_SCHEME) ||
                    Objects.equals(uri.getScheme(), DOCS.IPFS_SCHEME)) {

                webView.getSettings().setJavaScriptEnabled(false);
                DOCS.cleanupResolver(uri);
                attachUri(uri);
            } else {
                webView.getSettings().setJavaScriptEnabled(
                        InitApplication.isJavascriptEnabled(requireContext())
                );
            }
            releaseSessions(Objects.requireNonNull(uri.getAuthority()).hashCode());
            webView.stopLoading();
            webView.loadUrl(uri.toString());
            updateActions();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cleanup();
    }

    private void download() {
        try {
            String url = webView.getUrl();
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), DOCS.IPFS_SCHEME) ||
                        Objects.equals(uri.getScheme(), DOCS.IPNS_SCHEME)) {
                    contentDownloader(uri);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void share() {
        try {
            Uri uri = Uri.parse(webView.getUrl());

            ComponentName[] names = {new ComponentName(requireContext(), MainActivity.class)};

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link));
            intent.putExtra(Intent.EXTRA_TEXT, uri.toString());
            intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


            Intent chooser = Intent.createChooser(intent, getText(R.string.share));
            chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names);
            startActivity(chooser);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void previousPage() {
        try {
            webView.stopLoading();
            webView.goBack();
            oldFavicon.set(null);
            tabIcon.setImageResource(R.drawable.cloud_download);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void nextPage() {
        try {
            webView.stopLoading();
            webView.goForward();
            oldFavicon.set(null);
            tabIcon.setImageResource(R.drawable.cloud_download);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void info() {
        try {
            View view = LayoutInflater.from(requireContext()).inflate(
                    R.layout.content_info, null);

            ImageView imageView = view.findViewById(R.id.qr_code_image);
            String title = getString(R.string.information);
            String message = getString(R.string.url_access);
            String uri = webView.getUrl();
            Objects.requireNonNull(uri);
            TextView page = view.findViewById(R.id.page);
            if (uri.isEmpty()) {
                page.setVisibility(View.GONE);
            } else {
                page.setText(uri);
            }


            Bitmap bitmap = DOCS.getQRCode(uri);
            imageView.setImageBitmap(bitmap);

            MaterialAlertDialogBuilder builder =
                    new MaterialAlertDialogBuilder(requireContext());

            builder.setTitle(title).setMessage(message).setView(view).create().show();


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private ActionMode.Callback createFindActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_find_action_mode, menu);

                MenuItem action_mode_find = menu.findItem(R.id.action_mode_find);
                TextInputEditText mEditText = (TextInputEditText) action_mode_find.getActionView();
                Objects.requireNonNull(mEditText);
                mEditText.setSingleLine();
                mEditText.setHint(R.string.enter_search);
                mEditText.setFocusable(true);
                mEditText.requestFocus();

                mEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        webView.findAllAsync(editable.toString());
                    }
                });

                mode.setTitle("0/0");

                webView.setFindListener((activeMatchOrdinal, numberOfMatches, isDoneCounting) -> {
                    try {
                        String result = activeMatchOrdinal + "/" + numberOfMatches;
                        mode.setTitle(result);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                int itemId = item.getItemId();

                if (itemId == R.id.action_mode_previous) {
                    try {
                        webView.findNext(false);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                    return true;
                } else if (itemId == R.id.action_mode_next) {
                    try {
                        webView.findNext(true);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                try {
                    webView.clearMatches();
                    webView.setFindListener(null);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                } finally {
                    actionMode = null;
                }
            }
        };

    }

    private void releaseActionMode() {
        try {
            if (actionMode != null) {
                actionMode.finish();
                actionMode = null;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void findPage() {
        try {
            actionMode = ((AppCompatActivity) requireActivity()).startSupportActionMode(
                    createFindActionModeCallback());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private Dialog getDialog() {
        if (widthWindowSizeClass == WindowSizeClass.MEDIUM) {
            return new SideSheetDialog(requireContext());
        } else {
            BottomSheetDialog dialog = new BottomSheetDialog(requireContext());
            BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            behavior.setPeekHeight(0);
            return dialog;
        }
    }

    private void history() {
        try {
            Dialog historyDialog = getDialog();
            historyDialog.setContentView(R.layout.fragment_history);

            RecyclerView history = historyDialog.findViewById(R.id.history);
            Objects.requireNonNull(history);

            LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
            layoutManager.setReverseLayout(true);
            history.setLayoutManager(layoutManager);
            HistoryAdapter mHistoryAdapter = new HistoryAdapter(item -> {

                try {
                    openUri(Uri.parse(item.getUrl()));
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                } finally {
                    historyDialog.dismiss();
                }
            }, webView.copyBackForwardList());
            history.setAdapter(mHistoryAdapter);
            historyDialog.show();

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @NonNull
    private String getUrl() {
        return Objects.requireNonNull(webView.getUrl());
    }

    @NonNull
    private String getTitle() {
        try {
            String title = webView.getTitle();
            if (title == null) {
                title = Uri.parse(getUrl()).getAuthority();
            }
            Objects.requireNonNull(title);
            return title;
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return "";
    }

    @Nullable
    private Bitmap getFavicon() {
        return webView.getFavicon();
    }

    private void setFavicon() {
        tabIcon.setImageResource(R.drawable.cloud_download);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        tabItem = args.getLong(DOCS.TAB);

        computeWindowSizeClasses();

        PackageManager pm = requireActivity().getPackageManager();
        hasCamera = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);

        root = view.findViewById(R.id.browser_root);

        tabIcon = view.findViewById(R.id.tab_icon);
        tabTitle = view.findViewById(R.id.tab_title);

        MaterialCardView tabSite = view.findViewById(R.id.tab_site);
        tabSite.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            try {
                InitApplication.Engine searchEngine = InitApplication.getEngine(requireContext());
                openUri(searchEngine.uri());
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        View tabIdent = view.findViewById(R.id.tab_ident);
        tabIdent.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();
            info();
        });

        home = view.findViewById(R.id.action_home);

        home.setOnClickListener(v -> ((MainActivity) requireActivity()).openTabsDialog());


        // back page
        previousPage = view.findViewById(R.id.action_previous_page);
        previousPage.setOnClickListener(v -> previousPage());


        // next page
        nextPage = view.findViewById(R.id.action_next_page);
        nextPage.setOnClickListener(v -> nextPage());


        // reload page
        reloadPage = view.findViewById(R.id.action_reload_page);
        reloadPage.setOnClickListener(v -> reloadPage());


        MaterialButton bookmarks = view.findViewById(R.id.action_bookmarks);
        bookmarks.setOnClickListener(v -> {
            try {
                BookmarksFragment.newInstance(tabItem)
                        .show(getParentFragmentManager(), BookmarksFragment.TAG);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        MaterialButton actionOverflow = view.findViewById(R.id.action_overflow);
        actionOverflow.setOnClickListener(v -> {
            try {
                LayoutInflater inflater = (LayoutInflater)
                        requireContext().getSystemService(LAYOUT_INFLATER_SERVICE);


                View menuOverflow = inflater.inflate(
                        R.layout.menu_overflow, root, false);


                PopupWindow dialog = new PopupWindow(
                        requireContext(), null, android.R.attr.contextPopupMenuStyle);
                dialog.setContentView(menuOverflow);
                dialog.setOutsideTouchable(true);
                dialog.setFocusable(true);

                dialog.showAsDropDown(root, 0, 0, Gravity.TOP | Gravity.END);


                MaterialButton actionNextPage = menuOverflow.findViewById(R.id.action_next_page);

                if (widthWindowSizeClass == WindowSizeClass.MEDIUM) {
                    actionNextPage.setVisibility(View.GONE);
                } else {
                    actionNextPage.setVisibility(View.VISIBLE);
                }


                actionNextPage.setEnabled(webView.canGoForward());

                actionNextPage.setOnClickListener(v1 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        nextPage();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                MaterialButton actionBookmark = menuOverflow.findViewById(R.id.action_bookmark);

                boolean hasBookmark = hasBookmark();
                if (hasBookmark) {
                    actionBookmark.setIconResource(R.drawable.star);
                } else {
                    actionBookmark.setIconResource(R.drawable.star_outline);
                }

                actionBookmark.setOnClickListener(v1 -> {

                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        bookmark();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });

                Button actionFindPage = menuOverflow.findViewById(R.id.action_find_page);

                actionFindPage.setOnClickListener(v12 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        findPage();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionDownload = menuOverflow.findViewById(R.id.action_download);

                actionDownload.setEnabled(downloadActive());

                actionDownload.setOnClickListener(v13 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        download();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionShare = menuOverflow.findViewById(R.id.action_share);

                actionShare.setOnClickListener(v14 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        share();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionInformation = menuOverflow.findViewById(R.id.action_information);

                actionInformation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        info();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionScan = menuOverflow.findViewById(R.id.action_scan);
                if (!hasCamera) {
                    actionScan.setVisibility(View.GONE);
                }
                actionScan.setOnClickListener(vv -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);


                        if (ContextCompat.checkSelfPermission
                                (requireContext(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
                            return;
                        }

                        invokeScan();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });


                TextView actionHistory = menuOverflow.findViewById(R.id.action_history);
                actionHistory.setOnClickListener(v16 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        history();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionEditUri = menuOverflow.findViewById(R.id.action_edit_uri);
                actionEditUri.setOnClickListener(v15 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        try {

                            final View form = ((LayoutInflater)
                                    requireActivity().getSystemService(LAYOUT_INFLATER_SERVICE))
                                    .inflate(R.layout.uri_enter, null);

                            final EditText input = form.findViewById(R.id.enterUri);

                            MaterialAlertDialogBuilder enterUriDialog = new MaterialAlertDialogBuilder(
                                    requireContext())
                                    .setTitle(R.string.enter_uri)
                                    .setView(form)
                                    .setCancelable(false)
                                    .setPositiveButton(android.R.string.ok, (dialogEnterUri, whichButton) -> {

                                        String origin = "";
                                        try {
                                            origin = input.getText().toString();
                                            if (!origin.isBlank()) {
                                                String text = origin;

                                                // simple checks if text starts with "/ipns/ or "/ipfs/" replace
                                                // it with "ipns://" or "ipfs://"
                                                if (text.startsWith("/ipns/")) {
                                                    text = text.replaceFirst("/ipns/",
                                                            "ipns://");
                                                }
                                                if (text.startsWith("/ipfs/")) {
                                                    text = text.replaceFirst("/ipfs/",
                                                            "ipfs://");
                                                }

                                                Uri uri = Uri.parse(text);
                                                Objects.requireNonNull(uri);
                                                String scheme = uri.getScheme();
                                                if (!Objects.equals(scheme, "ipns") &&
                                                        !Objects.equals(scheme, "ipfs") &&
                                                        !Objects.equals(scheme, "http") &&
                                                        !Objects.equals(scheme, "https")) {
                                                    throw new IllegalArgumentException();
                                                }
                                                openUri(uri);
                                            }
                                        } catch (Throwable throwable) {
                                            String message = getString(R.string.invalid_uri);
                                            message = message.concat(" : ").concat(origin);
                                            EVENTS.getInstance(requireContext()).fatal(message);
                                        } finally {
                                            dialogEnterUri.dismiss();
                                        }

                                    })

                                    .setNegativeButton(android.R.string.cancel,
                                            (dialogCancel, whichButton) -> dialogCancel.dismiss());


                            enterUriDialog.show();

                        } catch (Throwable e) {
                            LogUtils.error(TAG, e);
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });


                TextView actionDownloads = menuOverflow.findViewById(R.id.action_downloads);
                actionDownloads.setOnClickListener(v17 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        try {
                            mFolderRequestForResult.launch(InitApplication.getDownloadsIntent());
                        } catch (Throwable e) {
                            EVENTS.getInstance(requireContext()).warning(
                                    getString(R.string.no_activity_found_to_handle_uri));
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionCleanup = menuOverflow.findViewById(R.id.action_cleanup);
                actionCleanup.setOnClickListener(v18 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        try {
                            // clear web view data
                            clearWebView();

                            // Clear data and cookies
                            BrowserResetWorker.reset(requireContext());
                        } finally {
                            EVENTS.getInstance(requireContext())
                                    .warning(getString(R.string.browser_cleanup_message));
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionSettings = menuOverflow.findViewById(R.id.action_settings);
                actionSettings.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        showSettings();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionDocumentation = menuOverflow.findViewById(R.id.action_documentation);
                actionDocumentation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        showDocumentation();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        progressIndicator = view.findViewById(R.id.progress_circle);
        progressIndicator.setVisibility(View.INVISIBLE);
        webView = view.findViewById(R.id.web_view);
        swipeRefreshLayout = view.findViewById(R.id.swipe_container);

        MaterialTextView offline_mode = view.findViewById(R.id.offline_mode);


        InitApplication.setWebSettings(webView,
                InitApplication.isJavascriptEnabled(requireContext()));

        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, false);


        swipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                swipeRefreshLayout.setRefreshing(true);
                reloadPage();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        CustomWebChromeClient mCustomWebChromeClient =
                new CustomWebChromeClient(requireActivity(), this);
        webView.setWebChromeClient(mCustomWebChromeClient);


        webView.setDownloadListener((url, userAgent, contentDisposition, mimeType, contentLength) -> {

            try {

                String filename = URLUtil.guessFileName(url, contentDisposition, mimeType);
                Uri uri = Uri.parse(url);

                if (Objects.equals(uri.getScheme(), DOCS.IPFS_SCHEME) ||
                        Objects.equals(uri.getScheme(), DOCS.IPNS_SCHEME)) {
                    String res = uri.getQueryParameter("download");
                    if (Objects.equals(res, "0")) {
                        try {
                            EVENTS.getInstance(requireContext())
                                    .warning(getString(R.string.browser_handle_file, filename));
                        } finally {
                            progressIndicator.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        contentDownloader(uri);
                    }
                } else {
                    fileDownloader(uri, filename, mimeType, contentLength);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        ThorModel thorModel =
                new ViewModelProvider(requireActivity()).get(ThorModel.class);


        thorModel.getTabs().observe(getViewLifecycleOwner(), tabs -> {
            if (tabs != null) {
                int size = tabs.size();
                int resId = R.drawable.numeric_9_plus_box_multiple_outline;
                if (size < 10) {
                    switch (size) {
                        case 0 -> resId = R.drawable.numeric_0_box_multiple_outline;
                        case 1 -> resId = R.drawable.numeric_1_box_multiple_outline;
                        case 2 -> resId = R.drawable.numeric_2_box_multiple_outline;
                        case 3 -> resId = R.drawable.numeric_3_box_multiple_outline;
                        case 4 -> resId = R.drawable.numeric_4_box_multiple_outline;
                        case 5 -> resId = R.drawable.numeric_5_box_multiple_outline;
                        case 6 -> resId = R.drawable.numeric_6_box_multiple_outline;
                        case 7 -> resId = R.drawable.numeric_7_box_multiple_outline;
                        case 8 -> resId = R.drawable.numeric_8_box_multiple_outline;
                        case 9 -> resId = R.drawable.numeric_9_box_multiple_outline;
                    }
                }
                home.setIconResource(resId);
            }
        });
        thorModel.online().observe(getViewLifecycleOwner(), (online) -> {
            try {
                if (online != null) {
                    if (online) {
                        offline_mode.setVisibility(View.GONE);
                        reloadPage();
                    } else {
                        offline_mode.setVisibility(View.VISIBLE);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        webView.setWebViewClient(new WebViewClient() {

            private final Map<Uri, Boolean> loadedUrls = new HashMap<>();
            private final AtomicReference<String> host = new AtomicReference<>();


            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request,
                                            WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                LogUtils.error(TAG, "onReceivedHttpError " + errorResponse.getReasonPhrase());
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                LogUtils.info(TAG, "onReceivedSslError " + error.toString());
            }


            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                LogUtils.info(TAG, "onPageCommitVisible " + url);
                progressIndicator.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {

                try {

                    WebViewDatabase database = WebViewDatabase.getInstance(requireContext());
                    String[] data = database.getHttpAuthUsernamePassword(host, realm);


                    String storedName = null;
                    String storedPass = null;

                    if (data != null) {
                        storedName = data[0];
                        storedPass = data[1];
                    }

                    LayoutInflater inflater = (LayoutInflater)
                            requireActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
                    final View form = inflater.inflate(R.layout.http_auth_request, null);


                    final EditText usernameInput = form.findViewById(R.id.user_name);
                    final EditText passwordInput = form.findViewById(R.id.password);

                    if (storedName != null) {
                        usernameInput.setText(storedName);
                    }

                    if (storedPass != null) {
                        passwordInput.setText(storedPass);
                    }

                    MaterialAlertDialogBuilder authDialog = new MaterialAlertDialogBuilder(
                            requireContext())
                            .setTitle(R.string.authentication)
                            .setView(form)
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.ok, (dialog, whichButton) -> {

                                String username = usernameInput.getText().toString();
                                String password = passwordInput.getText().toString();

                                database.setHttpAuthUsernamePassword(host, realm, username, password);

                                handler.proceed(username, password);
                                dialog.dismiss();
                            })

                            .setNegativeButton(android.R.string.cancel, (dialog, whichButton) -> {
                                dialog.dismiss();
                                view.stopLoading();
                                handler.cancel();
                            });


                    authDialog.show();
                    return;
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                super.onReceivedHttpAuthRequest(view, handler, host, realm);
            }


            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                LogUtils.info(TAG, "onLoadResource : " + url);
            }

            @Override
            public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
                super.doUpdateVisitedHistory(view, url, isReload);
                LogUtils.info(TAG, "doUpdateVisitedHistory : " + url + " " + isReload);
            }

            @Override
            public void onPageStarted(WebView view, String uri, Bitmap favicon) {
                LogUtils.info(TAG, "onPageStarted : " + uri);

                progressIndicator.setVisibility(View.VISIBLE);
                releaseActionMode();
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                LogUtils.info(TAG, "onPageFinished : " + url);

                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), DOCS.IPNS_SCHEME) ||
                        Objects.equals(uri.getScheme(), DOCS.IPFS_SCHEME)) {

                    try {
                        if (numUris() == 0) {
                            progressIndicator.setVisibility(View.INVISIBLE);
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                } else {
                    progressIndicator.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                LogUtils.error(TAG, "onReceivedError " + view.getUrl() + " " +
                        error.getDescription() + " " + error.getErrorCode());

                if (error.getErrorCode() == -2) {
                    oldFavicon.set(null);
                    tabIcon.setImageResource(R.drawable.cloud_cancel);
                }
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                try {
                    Uri uri = request.getUrl();

                    LogUtils.error(TAG, "shouldOverrideUrlLoading : " + uri);

                    if (Objects.equals(uri.getScheme(), ABOUT_SCHEME)) {
                        return true;
                    } else if (Objects.equals(uri.getScheme(), DOCS.HTTP_SCHEME)) {

                        Uri redirectUri = DOCS.redirectHttp(uri);
                        if (!Objects.equals(redirectUri, uri)) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, redirectUri,
                                    requireContext(), MainActivity.class);
                            startActivity(intent);
                            return true;
                        }
                        return false;
                    } else if (Objects.equals(uri.getScheme(), DOCS.HTTPS_SCHEME)) {
                        if (InitApplication.isRedirectUrlEnabled(requireContext())) {
                            Uri redirectUri = DOCS.redirectHttps(uri);
                            if (!Objects.equals(redirectUri, uri)) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, redirectUri,
                                        requireContext(), MainActivity.class);
                                startActivity(intent);
                                return true;
                            }
                        }
                        return false;
                    } else if (Objects.equals(uri.getScheme(), DOCS.MAGNET_SCHEME)) {
                        magnetDownloader(uri);
                        return true;
                    } else if (Objects.equals(uri.getScheme(), DOCS.IPNS_SCHEME) ||
                            Objects.equals(uri.getScheme(), DOCS.IPFS_SCHEME)) {

                        String res = uri.getQueryParameter("download");
                        if (Objects.equals(res, "1")) {
                            contentDownloader(uri);
                            return true;
                        }

                        progressIndicator.setVisibility(View.VISIBLE);
                        return false;
                    } else {
                        // all other stuff
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } catch (Throwable ignore) {
                            LogUtils.error(TAG, "Not  handled uri " + uri);
                        }
                        return true;
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                return false;

            }


            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

                Uri uri = request.getUrl();

                LogUtils.error(TAG, "shouldInterceptRequest : " + uri.toString());

                host.set(uri.getHost());
                if (Objects.equals(uri.getScheme(), DOCS.HTTP_SCHEME) ||
                        Objects.equals(uri.getScheme(), DOCS.HTTPS_SCHEME)) {
                    boolean advertisement = false;
                    if (!loadedUrls.containsKey(uri)) {
                        advertisement = AdBlocker.isAd(uri);
                        loadedUrls.put(uri, advertisement);
                    } else {
                        Boolean value = loadedUrls.get(uri);
                        if (value != null) {
                            advertisement = value;
                        }
                    }

                    if (advertisement) {
                        try {
                            return DOCS.createEmptyResource();
                        } catch (Throwable throwable) {
                            return null;
                        }
                    } else {
                        return null;
                    }

                } else if (Objects.equals(uri.getScheme(), DOCS.IPNS_SCHEME) ||
                        Objects.equals(uri.getScheme(), DOCS.IPFS_SCHEME)) {

                    try {
                        DOCS docs = DOCS.getInstance(requireContext());
                        attachUri(uri);

                        int authority = Objects.requireNonNull(uri.getAuthority()).hashCode();
                        Session session = getSession(authority);

                        Cancellable cancellable = () -> !hasSession(authority);

                        try {
                            Uri redirectUri = uri;
                            if (InitApplication.isRedirectIndexEnabled(requireContext())) {
                                redirectUri = docs.redirectUri(session, uri, cancellable);
                                if (!Objects.equals(uri, redirectUri)) {
                                    return DOCS.createRedirectMessage(redirectUri);
                                }
                            }
                            return docs.getResponse(session, redirectUri, cancellable);

                        } catch (Throwable throwable) {
                            if (cancellable.isCancelled()) {
                                return DOCS.createEmptyResource();
                            }
                            return DOCS.createErrorMessage(throwable);
                        } finally {
                            detachUri(uri);
                        }
                    } catch (Throwable throwable) {
                        return DOCS.createErrorMessage(throwable);
                    }
                }
                return null;
            }
        });

        visibilityActions(); // check of actions are visible

        try {
            TABS tabs = TABS.getInstance(requireContext());

            Tab tab = tabs.getTab(tabItem);
            Objects.requireNonNull(tab);
            String url = tab.uri();
            Objects.requireNonNull(url);

            updateTitle(tab.title(), url);

            Uri uri = Uri.parse(url);
            openUri(uri);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        if (savedInstanceState != null) {
            webView.restoreState(savedInstanceState);
        }
    }

    private void invokeScan() {
        try {
            PackageManager pm = requireActivity().getPackageManager();

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                ScanOptions options = new ScanOptions();
                options.setDesiredBarcodeFormats(ScanOptions.ALL_CODE_TYPES);
                options.setPrompt(getString(R.string.scan_qrcode));
                options.setCameraId(0);  // Use a specific camera of the device
                options.setBeepEnabled(true);
                options.setOrientationLocked(false);
                mScanRequestForResult.launch(options);
            } else {
                EVENTS.getInstance(requireContext()).permission(
                        getString(R.string.feature_camera_required));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public enum WindowSizeClass {COMPACT, MEDIUM}
}
