package threads.thor.fragments;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.sidesheet.SideSheetDialog;

import java.util.Comparator;
import java.util.Objects;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.core.DOCS;
import threads.thor.core.books.BOOKS;
import threads.thor.core.books.Bookmark;
import threads.thor.core.events.EVENTS;
import threads.thor.core.tabs.TABS;
import threads.thor.model.ThorModel;
import threads.thor.utils.BookmarksAdapter;

public class BookmarksFragment extends DialogFragment {

    public static final String TAG = BookmarksFragment.class.getSimpleName();
    private WindowSizeClass widthWindowSizeClass = WindowSizeClass.COMPACT;

    public static BookmarksFragment newInstance(long tabIdx) {
        BookmarksFragment bookmarksFragment = new BookmarksFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(DOCS.TAB, tabIdx);
        bookmarksFragment.setArguments(bundle);
        return bookmarksFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booksmark, container, false);

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        long tabIdx = args.getLong(DOCS.TAB);


        RecyclerView bookmarks = view.findViewById(R.id.bookmarks);
        Objects.requireNonNull(bookmarks);
        bookmarks.setLayoutManager(new LinearLayoutManager(requireContext()));

        ThorModel thorModel =
                new ViewModelProvider(requireActivity()).get(ThorModel.class);

        BookmarksAdapter mBookmarksAdapter = new BookmarksAdapter(
                new BookmarksAdapter.BookmarkListener() {
                    @Override
                    public void onClick(@NonNull Bookmark bookmark) {
                        try {
                            if (tabIdx == 0L) {
                                TABS tabs = TABS.getInstance(requireContext());
                                tabs.createTab(bookmark.title(), Uri.parse(bookmark.uri()),
                                        bookmark.bitmap());
                            } else {
                                thorModel.uri(Uri.parse(bookmark.uri()));
                            }
                            Objects.requireNonNull(getDialog()).dismiss();
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }

                    @Override
                    public void onDismiss(@NonNull Bookmark bookmark) {
                        try {
                            String name = bookmark.title();
                            BOOKS books = BOOKS.getInstance(requireContext());
                            books.removeBookmark(bookmark);

                            EVENTS.getInstance(requireContext()).info(
                                    requireContext().getString(R.string.bookmark_removed, name));
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }
                });
        bookmarks.setAdapter(mBookmarksAdapter);


        thorModel.getBookmarks().observe(getViewLifecycleOwner(), (marks -> {
            try {
                if (marks != null) {
                    marks.sort(Comparator.comparing(Bookmark::timestamp).reversed());
                    mBookmarksAdapter.updateData(marks);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }));

        return view;
    }

    private void computeWindowSizeClasses() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;


        if (widthDp < 600f) {
            widthWindowSizeClass = WindowSizeClass.COMPACT;
        } else {
            widthWindowSizeClass = WindowSizeClass.MEDIUM;
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        computeWindowSizeClasses();
        if (widthWindowSizeClass == WindowSizeClass.MEDIUM) {
            return new SideSheetDialog(requireContext());
        } else {
            BottomSheetDialog dialog = new BottomSheetDialog(requireContext());
            BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            behavior.setPeekHeight(0);
            return dialog;
        }
    }

    public enum WindowSizeClass {COMPACT, MEDIUM}
}
