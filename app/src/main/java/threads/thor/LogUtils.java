package threads.thor;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;

public class LogUtils {

    @SuppressWarnings("SameReturnValue")
    private static boolean isDebug() {
        return false;
    }


    public static void info(@Nullable final String tag, @NonNull String message) {
        if (isDebug()) {
            Log.i(tag, message);
        }
    }

    public static void error(@Nullable final String tag, @Nullable String message) {
        if (isDebug()) {
            Log.e(tag, Objects.requireNonNullElse(message, "No error message defined"));
        }
    }

    public static void error(final String tag, @NonNull Throwable throwable) {
        if (isDebug()) {
            Log.e(tag, throwable.getLocalizedMessage(), throwable);
        }
    }
}
