package threads.thor;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.sidesheet.SideSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import threads.lite.cid.Cid;
import threads.thor.core.DOCS;
import threads.thor.core.events.EVENTS;
import threads.thor.core.tabs.TABS;
import threads.thor.core.tabs.Tab;
import threads.thor.fragments.BrowserFragment;
import threads.thor.model.EventsModel;
import threads.thor.model.ThorModel;
import threads.thor.utils.MimeTypeService;
import threads.thor.utils.TabDiffCallback;
import threads.thor.utils.TabsAdapter;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private final ActivityResultLauncher<String> mPermissionNotificationResult = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            result -> {
            });
    private ViewPager2 browserPager;
    private ViewPagerFragmentAdapter browserAdapter;
    private ThorModel thorModel;
    private LinearLayout linearLayout;
    private Dialog tabsDialog;

    private void notificationRequest() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    android.Manifest.permission.POST_NOTIFICATIONS)
                    != PackageManager.PERMISSION_GRANTED) {
                mPermissionNotificationResult.launch(Manifest.permission.POST_NOTIFICATIONS);
            }
        }
    }


    private void openUri(@NonNull Uri uri) {
        try {
            TABS tabs = TABS.getInstance(getApplicationContext());

            tabs.createTab(DOCS.getFileName(uri), uri, null);
            closeTabsDialog();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void openTabsDialog() {

        tabsDialog = new SideSheetDialog(this);
        tabsDialog.setContentView(R.layout.fragment_tabs);


        ThorModel thorModel = new ViewModelProvider(this).get(ThorModel.class);

        RecyclerView recyclerView = tabsDialog.findViewById(R.id.tabs_recycler_view);
        Objects.requireNonNull(recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(linearLayoutManager);

        TabsAdapter tabsAdapter = new TabsAdapter(new TabsAdapter.ViewHolderListener() {
            @Override
            public void onItemClicked(int position) {
                try {
                    browserPager.setCurrentItem(position, true);
                    closeTabsDialog();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            @Override
            public void onItemDismiss(Tab tab) {
                try {
                    TABS tabs = TABS.getInstance(getApplicationContext());
                    tabs.removeTab(tab);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            @Override
            public boolean isSelected(int position) {
                return browserPager.getCurrentItem() == position;
            }
        });

        recyclerView.setAdapter(tabsAdapter);


        thorModel.getTabs().observe(this, tabs -> {
            if (tabs != null) {
                tabsAdapter.updateData(tabs);
                int position = browserPager.getCurrentItem();
                linearLayoutManager.scrollToPosition(position);
            }
        });


        MaterialToolbar gridToolbar = tabsDialog.findViewById(R.id.tabs_toolbar);
        Objects.requireNonNull(gridToolbar);
        gridToolbar.getMenu().findItem(R.id.add_tab).setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.add_tab) {
                try {
                    TABS tabs = TABS.getInstance(getApplicationContext());
                    InitApplication.Engine searchEngine =
                            InitApplication.getEngine(getApplicationContext());
                    tabs.createTab(searchEngine.name(), searchEngine.uri(), null);
                    closeTabsDialog();
                    return true;
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
            return false;
        });

        gridToolbar.getMenu().findItem(R.id.close_tabs).setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.close_tabs) {
                TABS tabs = TABS.getInstance(getApplicationContext());
                tabs.clear();
                return true;
            }
            return false;
        });

        tabsDialog.setOnDismissListener(dialog -> {
            TABS tabs = TABS.getInstance(getApplicationContext());
            if (!tabs.hasTabs()) {
                InitApplication.Engine searchEngine =
                        InitApplication.getEngine(getApplicationContext());
                tabs.createTab(searchEngine.name(), searchEngine.uri(), null);
            }
        });

        tabsDialog.show();
    }

    private void closeTabsDialog() {
        if (tabsDialog != null) {
            tabsDialog.dismiss();
            tabsDialog = null;
        }
    }

    @Nullable
    private BrowserFragment getBrowserFragment() {
        long tabItem = browserAdapter.getItemId(browserPager.getCurrentItem());
        if (tabItem > 0L) {
            Optional<Fragment> result = getSupportFragmentManager().getFragments()
                    .stream()
                    .filter(it -> it instanceof BrowserFragment && it.getArguments() != null
                            && it.getArguments().getLong(DOCS.TAB) == tabItem)
                    .findFirst();
            return (BrowserFragment) result.orElse(null);
        }
        return null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                BrowserFragment browserFragment = getBrowserFragment();
                if (browserFragment != null) {
                    boolean result = browserFragment.onBackPressedCheck();
                    if (result) {
                        return;
                    }
                }
                finish();
            }
        });


        browserPager = findViewById(R.id.browser_pager);


        browserAdapter = new ViewPagerFragmentAdapter(
                getSupportFragmentManager(), getLifecycle());
        browserPager.setAdapter(browserAdapter);
        browserPager.setUserInputEnabled(false);
        browserPager.setOffscreenPageLimit(20);


        linearLayout = findViewById(R.id.main_layout);


        thorModel = new ViewModelProvider(this).get(ThorModel.class);

        thorModel.uri().observe(this, uri -> {
            try {
                BrowserFragment browserFragment = getBrowserFragment();
                if (browserFragment != null) {
                    browserFragment.openUri(uri);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        thorModel.getTabs().observe(this, tabs -> {
            if (tabs != null) {
                int count = browserAdapter.getItemCount();
                browserAdapter.updateData(tabs);

                if (tabs.size() > count) {
                    browserPager.setCurrentItem(tabs.size(), true);
                }

            }
        });


        EventsModel eventsModel =
                new ViewModelProvider(this).get(EventsModel.class);

        eventsModel.fatal().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.content();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(linearLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.show();
                    }
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventsModel.error().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.content();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(linearLayout, content, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    eventsModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventsModel.warning().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.content();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(linearLayout, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventsModel.permission().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.content();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(linearLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.settings, view -> {
                            final Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" +
                                    view.getContext().getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            view.getContext().startActivity(intent);
                        });
                        snackbar.show();

                    }
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventsModel.info().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.content();
                    if (!content.isEmpty()) {
                        Toast.makeText(MainActivity.this,
                                content, Toast.LENGTH_SHORT).show();
                    }
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        Intent intent = getIntent();

        if (!handleIntents(intent)) {
            // show the default page in case nothing is defined
            try {
                TABS tabs = TABS.getInstance(getApplicationContext());
                boolean hasTabs = tabs.hasTabs();
                if (!hasTabs) {
                    InitApplication.Engine searchEngine =
                            InitApplication.getEngine(getApplicationContext());
                    tabs.createTab(searchEngine.name(), searchEngine.uri(), null);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        thorModel.online(threads.lite.cid.Network.isNetworkConnected(getApplicationContext()));

        notificationRequest(); // todo check it
    }


    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        LogUtils.error(TAG, "onNewIntent");
        setIntent(intent);
        handleIntents(intent);
    }


    private boolean handleIntents(Intent intent) {

        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            Uri uri = intent.getData();
            if (uri != null) {
                try {
                    LogUtils.error(TAG, uri.toString());
                    openUri(uri);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            }
        }

        if (Intent.ACTION_SEND.equals(action)) {
            if (Objects.equals(intent.getType(), MimeTypeService.PLAIN_MIME_TYPE)) {
                String text = intent.getStringExtra(Intent.EXTRA_TEXT);
                return doSearch(text);
            }
        }

        return false;
    }


    private boolean doSearch(@Nullable String query) {
        try {
            if (query != null && !query.isEmpty()) {
                Uri uri = Uri.parse(query);
                String scheme = uri.getScheme();
                if (Objects.equals(scheme, DOCS.IPNS_SCHEME) ||
                        Objects.equals(scheme, DOCS.IPFS_SCHEME) ||
                        Objects.equals(scheme, DOCS.HTTP_SCHEME) ||
                        Objects.equals(scheme, DOCS.HTTPS_SCHEME)) {
                    openUri(uri);
                } else {

                    try {
                        // in case the search is an ipns string
                        openUri(Uri.parse(DOCS.IPNS_SCHEME + "://" + Cid.decode(query)));
                    } catch (Throwable ignore) {
                        EVENTS.getInstance(getApplicationContext())
                                .error(query);
                    }
                }
                return true;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        networkCallback();
    }


    private void networkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);

            ConnectivityManager.NetworkCallback networkCallback =
                    new ConnectivityManager.NetworkCallback() {
                        @Override
                        public void onAvailable(@NonNull Network network) {
                            super.onAvailable(network);
                            thorModel.online(true);
                        }

                        @Override
                        public void onLost(@NonNull Network network) {
                            super.onLost(network);
                            thorModel.online(false);
                        }

                    };

            NetworkRequest networkRequest = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                    .build();

            connectivityManager.registerNetworkCallback(networkRequest, networkCallback);


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    static class ViewPagerFragmentAdapter extends FragmentStateAdapter {
        private final List<Tab> tabs = new ArrayList<>();


        ViewPagerFragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
            super(fragmentManager, lifecycle);
        }

        @Override
        public long getItemId(int position) {
            if (position < 0 || position >= getItemCount()) {
                return 0;
            }
            return tabs.get(position).idx();
        }

        @Override
        public boolean containsItem(long tabIdx) {
            for (Tab tab : tabs) {
                if (tab.idx() == tabIdx) {
                    return true;
                }
            }
            return false;
        }


        void updateData(@NonNull List<Tab> tabs) {
            final TabDiffCallback diffCallback = new TabDiffCallback(this.tabs, tabs);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

            this.tabs.clear();
            this.tabs.addAll(tabs);
            diffResult.dispatchUpdatesTo(this);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            Tab tab = tabs.get(position);
            BrowserFragment browserFragment = new BrowserFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(DOCS.TAB, tab.idx());
            browserFragment.setArguments(bundle);
            return browserFragment;
        }

        @Override
        public int getItemCount() {
            return tabs.size();
        }

    }
}