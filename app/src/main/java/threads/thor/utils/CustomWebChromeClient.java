package threads.thor.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;

import threads.thor.LogUtils;
import threads.thor.MainActivity;
import threads.thor.fragments.BrowserFragment;

public class CustomWebChromeClient extends WebChromeClient {
    private static final String TAG = CustomWebChromeClient.class.getSimpleName();
    private final Activity activity;
    private final BrowserFragment browserFragment;

    private View mCustomView;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;

    public CustomWebChromeClient(@NonNull Activity activity,
                                 @NonNull BrowserFragment browserFragment) {
        this.activity = activity;
        this.browserFragment = browserFragment;
    }

    @Override
    public void onHideCustomView() {
        ((FrameLayout) activity.getWindow().getDecorView()).removeView(this.mCustomView);
        this.mCustomView = null;
        showSystemUI();
        this.mCustomViewCallback.onCustomViewHidden();
        this.mCustomViewCallback = null;
    }

    @Override
    public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, android.os.Message resultMsg) {
        WebView.HitTestResult result = view.getHitTestResult();
        String data = result.getExtra();
        Context context = view.getContext();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data), context, MainActivity.class);
        context.startActivity(browserIntent);
        return false;
    }

    @Override
    public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback) {
        this.mCustomView = paramView;
        this.mCustomViewCallback = paramCustomViewCallback;
        ((FrameLayout) activity.getWindow().getDecorView())
                .addView(this.mCustomView, new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
        hideSystemUI();

    }

    private void hideSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(activity.getWindow(), false);
        WindowInsetsControllerCompat controller = new WindowInsetsControllerCompat(
                activity.getWindow(), this.mCustomView);
        controller.hide(WindowInsetsCompat.Type.systemBars());
        controller.setSystemBarsBehavior(WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
    }

    private void showSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(activity.getWindow(), true);
        new WindowInsetsControllerCompat(activity.getWindow(), activity.getWindow().getDecorView())
                .show(WindowInsetsCompat.Type.systemBars());
    }

    @Override
    public Bitmap getDefaultVideoPoster() {
        return Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        super.onProgressChanged(view, newProgress);
    }


    @Override
    public void onReceivedTitle(WebView view, String title) {
        LogUtils.error(TAG, "onReceivedTitle " + title + " " + view.getUrl());
        browserFragment.updateTitle(title, view.getUrl());
    }

    @Override
    public void onReceivedIcon(WebView view, Bitmap icon) {
        LogUtils.error(TAG, "onReceivedIcon " + view.getUrl());
        browserFragment.updateFavicon(icon);
    }

}
