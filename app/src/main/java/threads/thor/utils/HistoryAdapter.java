package threads.thor.utils;

import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebBackForwardList;
import android.webkit.WebHistoryItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.core.DOCS;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private static final String TAG = HistoryAdapter.class.getSimpleName();
    private final HistoryListener listener;
    private final WebBackForwardList webBackForwardList;

    public HistoryAdapter(@NonNull HistoryListener listener, @NonNull WebBackForwardList list) {
        this.listener = listener;
        this.webBackForwardList = list;
    }


    @Override
    public int getItemViewType(int position) {
        return R.layout.card_history;
    }

    @Override
    @NonNull
    public HistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(v, listener);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WebHistoryItem history = webBackForwardList.getItemAtIndex(position);
        holder.onBind(history);
    }


    @Override
    public int getItemCount() {
        return webBackForwardList.getSize();
    }


    public interface HistoryListener {
        void onClick(@NonNull WebHistoryItem item);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView uri;
        final TextView title;
        final ImageView image;
        private WebHistoryItem history;

        private ViewHolder(View v, @NonNull HistoryListener listener) {
            super(v);

            MaterialCardView cardView = itemView.findViewById(R.id.card_view);
            this.title = itemView.findViewById(R.id.history_title);
            this.uri = itemView.findViewById(R.id.history_uri);
            this.image = itemView.findViewById(R.id.history_image);

            cardView.setOnClickListener((view) -> listener.onClick(history));
        }

        void onBind(@NonNull WebHistoryItem history) {

            this.history = history;
            try {
                String title = history.getTitle();
                this.title.setText(title);
                this.uri.setText(history.getUrl());

                Bitmap image = history.getFavicon();
                if (image != null) {
                    this.image.setImageBitmap(image);
                } else {
                    this.image.setImageResource(DOCS.getImageResource(Uri.parse(history.getUrl())));
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        }

    }
}
